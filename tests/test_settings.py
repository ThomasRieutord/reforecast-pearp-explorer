#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Unitary tests of the functions in ../reforecast_pearp_explorer/settings.py
Must be executed inside the `tests/` directory
"""

import os
import random
import datetime as dt
from reforecast_pearp_explorer import settings

rfs = settings.ReforecastSettings()

def test_add_vlevel_to_fieldnames():
    lfn = ["fieldname1", "fieldname2"]
    lvlvl = ["30", "4.1", "a"]
    vlvl_unit = "onions"
    
    trueRes = [
        "fieldname1_at_30_onions",
        "fieldname1_at_4.1_onions",
        "fieldname1_at_a_onions",
        "fieldname2_at_30_onions",
        "fieldname2_at_4.1_onions",
        "fieldname2_at_a_onions",
    ]
    
    res = rfs.add_vlevel_to_fieldnames(lfn,lvlvl,vlvl_unit)
    
    assert set(res)==set(trueRes)


def test_add_vlevel_to_field2gribid():
    lfn = ["fieldname1", "fieldname2"]
    lvlvl = ["30", "4.1", "a"]
    vlvl_unit = "hPa"
    
    fd2gid = {
        "fieldname1":{"dummyParam":2},
        "fieldname2":{"dummyParam":20},
    }
    trueRes = {
        "fieldname1_at_30_hPa":{"dummyParam":2, "level":"30", "typeOfFirstFixedSurface":100},
        "fieldname1_at_4.1_hPa":{"dummyParam":2, "level":"4.1", "typeOfFirstFixedSurface":100},
        "fieldname1_at_a_hPa":{"dummyParam":2, "level":"a", "typeOfFirstFixedSurface":100},
        "fieldname2_at_30_hPa":{"dummyParam":20, "level":"30", "typeOfFirstFixedSurface":100},
        "fieldname2_at_4.1_hPa":{"dummyParam":20, "level":"4.1", "typeOfFirstFixedSurface":100},
        "fieldname2_at_a_hPa":{"dummyParam":20, "level":"a", "typeOfFirstFixedSurface":100},
    }
    
    res = rfs.add_vlevel_to_field2gribid(fd2gid,lfn,lvlvl,vlvl_unit)
    
    assert res==trueRes


def test_get_2networks_dates():
    firstdate = dt.datetime(2000,3,1,18)
    lastdate = dt.datetime(2002,3,1,18)
    
    # True dates can be checked on OLIVE experiments
    first06date = dt.datetime(2000,3,4,6)
    last06date = dt.datetime(2002,2,27,6)
    first18date = dt.datetime(2000,3,1,18)
    last18date = dt.datetime(2002,3,1,18)
    
    d06,d18 = rfs.get_2networks_dates(firstdate, lastdate, dt.timedelta(hours = 60))
    
    
    assert d06[0] == first06date and d06[-1] == last06date and d18[0] == first18date and d18[-1] == last18date and all([d.hour==6 for d in d06]) and all([d.hour==18 for d in d18])


def test_get_xpid_from_rundate():
    
    rfs.build()
    
    trueXpids = ['GDM6','GDMX','GDL1','GDJ6','GDMK']
    rundates = [random.choice(rfs.xpid_rundates[xpid]) for xpid in trueXpids]
    
    xpids = []
    for rd in rundates:
        xpids.append(rfs.get_xpid_from_rundate(rd))
    
    assert xpids==trueXpids


def test_get_rundates_between_dates():
    
    trueDates = np.array([
        dt.datetime(2015, 1, 13, 6, 0),
        dt.datetime(2015, 1, 18, 6, 0),
        dt.datetime(2015, 2, 12, 6, 0),
        dt.datetime(2015, 2, 17, 6, 0),
        dt.datetime(2017, 1, 13, 6, 0),
        dt.datetime(2017, 1, 18, 6, 0),
    ])
    
    dates = rfs.get_rundates_between_dates(dt.datetime(2014, 12, 31, 18), dt.datetime(2017, 2, 1, 18), network="06", years=[2015, 2017], months=[1, 2], days=list(range(10,20)))
    
    assert dates == trueDates
