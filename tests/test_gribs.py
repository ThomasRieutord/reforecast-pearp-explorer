#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Unitary tests of the functions in ../reforecast_pearp_explorer/gribs.py
Must be executed inside the `tests/` directory
"""

import epygram
import netCDF4 as nc
from reforecast_pearp_explorer.gribs import *

gribtestfile = "/home/rieutordt/Donnees/GC04/grid.arpege-forecast.glob025+0018:00.grib"

def test_handgrip_matches():
    """Test that all the handgrips given for required fields identify a unique field"""
    go = epygram.formats.resource(gribtestfile, openmode="r")
    areOK =[]
    for fd in fields_to_check:
        hg = fields_to_handgrip[fd]
        isOK = handgrip_matches(hg, go)==1
        areOK.append(isOK)
    
    assert all(areOK)

def test_check_reqfields():
    go = epygram.formats.resource(gribtestfile, openmode="r")
    allOK = check_reqfields(go)
    assert allOK

def test_drop_to_netcdf():
    nctestfile = "../tmp/test.nc"
    
    drop_to_netcdf(gribtestfile, nctestfile)
    
    go = epygram.formats.resource(gribtestfile, openmode="r")
    fd0 = get_field(go, fields_to_check[0])
    nlat, nlon = fd0.geometry.get_datashape()
    
    ncf = nc.Dataset(nctestfile, mode="r")
    
    namesOK = set(ncf.variables.keys()) == set(fields_to_check + ['latitude', 'time', 'longitude'])
    
    areOK = []
    for fd in fields_to_check:
        isOK = ncf.variables[fd].shape == (nlat, nlon)
        areOK.append(isOK)
    
    ncf.close()
    
    assert all(areOK) and namesOK


# End of file
