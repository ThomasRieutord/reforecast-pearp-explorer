#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Unitary tests of the functions in ../reforecast_pearp_explorer/access.py
Must be executed inside the `tests/` directory
"""

from reforecast_pearp_explorer.access import *
import datetime as dt


def test_vortexpath_to_lustrepath():
    
    # vtxpath1 = '/home/rieutordt/vortex/fullpos/ecmwf2mf/G7Q8/20181006T1800P/fullpos/cpl.ifs-determ-prod.tl1198-c22+0000:00.fa'
    # true_lustrepath1 = '/cnrm/recyf/NO_SAVE/Data/users/rieutordt/reforecast-pearp-cy46t1/reanalysis/20181006T1800P/cpl.ifs-determ-prod.tl1198-c22+0000:00.fa'
    
    vtxpath2 = '/home/rieutordt/vortex/arpege/pearp/G7QB/20170220T1800P/mb002/forecast/grid.arpege-forecast.eurat01+0012:00.grib'
    true_lustrepath2 = "/cnrm/recyf/NO_SAVE/Data/users/rieutordt/reforecast-pearp-cy46t1/test/20170220T1800P/mb002/grid.arpege-forecast.eurat01+0012:00.grib"
    
    # lustrepath1 = vortexpath_to_lustrepath(vtxpath1, withfilename=True)
    lustrepath2 = vortexpath_to_lustrepath(vtxpath2, withfilename=True)
    
    assert lustrepath2==true_lustrepath2


def test_check_lustrepath():
    true_msg = '\nLongest common sub-path:\n/cnrm/recyf/NO_SAVE/Data/users/rieutordt/reforecast-pearp-cy43t2\n-reanalysis exist\n--20181011T1800P exist\n'
    localpath = '/cnrm/recyf/NO_SAVE/Data/users/rieutordt/reforecast-pearp-cy46t1/test/20181011T1800P/cpl.ifs-determ-prod.tl1198-c22+0000:00.fa'
    msg = check_lustrepath(localpath,mkdir =False)
    
    assert msg==true_msg


