import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="reforecast_pearp_explorer",
    version="0.1.1",
    author="Meteo-France",
    author_email="thomas.rieutord@meteo.fr",
    description="Access and exploit PEARP reforecast",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/ThomasRieutord/reforecast-pearp-explorer",
    packages=setuptools.find_packages(),
    classifiers=(
        "Environment :: Console"
        "Programming Language :: Python :: 3",
        "Operating System :: Linux",
        "Development Status :: 2 - Pre-Alpha",
    ),
)
