#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Tools to access the ARPEGE reforecast in cycle 43t2

@author: Rieutord Thomas
@date: 2021/06/02
"""


import os.path
import vortex
import olive
import epygram
import datetime as dt
from reforecast_pearp_explorer import access

N_members = access.N_members

def get_field_at_date(
    rundate,
    fieldname,
    domain="glob05",
    member=0,
    leadtime=0,
    vlevel=500
):
    """
    Fetch a 2-dimensional field at the specified date in the reforecast.
    
    Parameters
    ----------
    rundate: datetime.date
        Date at which the reforecast starts
    
    fieldname: str
        Name of the field to return. Must correspond to long names available
        in the GRIB files
    
    domain: str
        ID of the domain covered by the field
    
    member: int
        Number of the member from which it gets the field
    
    leadtime: int
        Forecast lead time. Number of hours after `runtime`. Therefore, the
        field is supposed to represent the atmosphere at the time `runtime+leadtime`
    
    vlevel: int
        Indicator the vertical level. Either hPa level or meter above ground,
        depending on the field.
    
    
    Returns
    -------
    field: ndarray of shape (nlat, nlon)
        Values of the requested field onto a lat-lon grid corresponding to the
        domain.
    """
    
    xpid = access.get_xpid_from_date(date)
    
    rhs=vortex.toolbox.input(
          kind="gridpoint",
          nativefmt='grib',
          origin='historic',
          geometry=domain,
          model='arpege',
          cutoff='prod',
          block="forecast",
          date=rundate.strftime("%Y%m%d")+'1800',
          term=leadtime,
          experiment=xpid,
          namespace='vortex.archive.fr',
          vapp='arpege',
          vconf='pearp',
          member=mbre,
          shouldfly=True,
          now=True,
          fatal=False
    )
    gribobj = rhs[0].contents.data
    lwf = gribobj.find_fields_in_resource({'name':fieldname,'level':vlevel})
    fd=gribobj.readfield(lwf[0])
    
    return fd.getdata()

def get_field_at_date_allmember_allleadtime(
    rundate,
    fieldname,
    domain="GLOB05",
    vlevel=500
):
    """
    Fetch 2-dimensional fields at the specified date of all members and all 
    leadtimes
    
    Parameters
    ----------
    rundate: datetime.date
        Date at which the reforecast starts
    
    fieldname: str
        Name of the field to return. Must correspond to long names available
        in the GRIB files
    
    domain: str
        ID of the domain covered by the field
    
    vlevel: int
        Indicator the vertical level. Either hPa level or meter above ground,
        depending on the field.
    
    
    Returns
    -------
    field: ndarray of shape (nlat, nlon)
        Values of the requested field onto a lat-lon grid corresponding to the
        domain.
    
    Example
    -------
    >>> from reforecast_pearp_explorer.access import get_field_at_date_allmember_allleadtime
    >>> import datetime as dt
    >>> rundate = dt.date(2017,7,17)
    >>> X = get_field_at_date_allmember_allleadtime(rundate,"Temperature")
    
    """
    
    maxleadtime = 108
    stride = 12
    leadtime = np.arange(0,maxleadtime + stride, stride)
    N_ech = leadtime.size
    
    xpid = access.get_xpid_from_date(rundate)
    
    fd_allmb = []
    for mb in range(N_members):
        print("mb" + str(mb).zfill(3))
        fd_allech = []
        for ech in range(N_ech):
            rhs=vortex.toolbox.input(
                  kind="gridpoint",
                  nativefmt='grib',
                  origin='historic',
                  geometry=domain,
                  model='arpege',
                  cutoff='prod',
                  block="forecast",
                  date=rundate.strftime("%Y%m%d")+'1800',
                  term= int(leadtime[ech]),
                  experiment=xpid,
                  namespace='vortex.archive.fr',
                  vapp='arpege',
                  vconf='pearp',
                  member=mb,
                  shouldfly=True,
                  now=True,
                  fatal=False
            )
            gribobj = rhs[0].contents.data
            lwf = gribobj.find_fields_in_resource({'name':fieldname,'level':vlevel})
            fd=gribobj.readfield(lwf[0])
            fd_allech.append(fd.getdata())
        fd_allmb.append(fd_allech)
    
    return np.array(fd_allmb)
