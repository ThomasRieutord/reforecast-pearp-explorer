#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Tools to access the long-term PEARP reforecast made at CNRM/GMAP/RECYF.
Module to ease the access of the reforecast outputs.
The data is originally on Meteo-France storage machine (Hendrix) and part of it is
downloaded on a local host (LUSTRE at CNRM).

@author: Rieutord Thomas
@creationdate: 2022/03/07
@history: Merge of hendrix.py and lustre.py modules

Global docstrings reviews:
  * 17 Mar. 2022 by Thomas Rieutord (TR)
"""

import string
import random
import sys
import time
import pickle
import os.path
import datetime as dt
import numpy as np
import vortex
import common  # This is usually a good idea
from bronx.stdtypes.date import Date
from reforecast_pearp_explorer.settings import ReforecastSettings

rfs = ReforecastSettings().build()

useremail = rfs.useremail
username = rfs.username

localbasepath = rfs.localbasepath

n_members = rfs.n_members
leadtimes = rfs.leadtimes
time_between_runs = rfs.time_between_runs

hendrixbasepath = "/home/" + username + "/vortex/"

def check_lustrepath(lustrepath, mkdir=False):
    """
    Check if all directories and sub-directories in this path exist.
    
    
    Parameters
    ----------
    lustrepath: str
        Path to a file in LUSTRE
        Example: '/cnrm/recyf/NO_SAVE/Data/users/rieutordt/reforecast-pearp-cy43t2/reanalysis/20181009T1800P/cpl.ifs-determ-prod.tl1198-c22+0000:00.fa'
    
    mkdir: bool
        If True, the functions creates the missing directories
    
    
    Returns
    -------
    msg: str
        Text message to track navigation in the directories
    
    
    Notes
    -----
    Last doctring review: 17 Mar. 2022 (TR)
    """
    msg = ""
    cwd = os.getcwd()
    
    cmpath = os.path.commonpath([lustrepath,localbasepath])
    msg += "\nLongest common sub-path:\n"
    msg += cmpath + "\n"
    
    tailpath = lustrepath.replace(cmpath, "")
    head, tail = os.path.split(tailpath) # Remove the file name
    head, tail = os.path.split(head)
    pathpieces = []
    while len(tail)>0:
        pathpieces.append(tail)
        head, tail = os.path.split(head)
    
    i=0
    cdir = cmpath
    os.chdir(cdir)
    for dirname in pathpieces[::-1]:
        i += 1
        cdir = os.path.join(cdir, dirname)
        if os.path.isdir(cdir):
            os.chdir(cdir)
            msg += "-"*i + dirname + " exist\n"
        else:
            msg += "-"*i + dirname + " DOES NOT exist\n"
            if mkdir:
                os.mkdir(dirname)
                os.chdir(dirname)
            else:
                break
    
    os.chdir(cwd)
    
    return msg


def count_files_per_rundate(
    startdate,
    enddate,
    domain,
    members=np.arange(n_members),
    ldtdex=np.arange(len(leadtimes)),
    prefixdir="test",
    fileformat="grib",
):
    """Check if the files covering a given time period exist.
    
    
    Parameters
    ----------
    startdate: datetime.datetime
        Starting date of the time period to be checked. It
        corresponds to the run date (not the validity date).
    
    enddate: datetime.datetime
        Ending date of the time period to be checked. It
        corresponds to the run date (not the validity date).
    
    domain: {"eurat01", "glob025"}
        Geographical domain
    
    members: array-like of int
        List of members to check.
    
    ldtdex: array-like of int
        List of lead time indices.
    
    prefixdir: str
        Directory name to distinguish several purpose of use in LUSTRE. Famous 
        purposes are
            "test" for the Oct. 2018 test of reforecast
            "full" for the full reforecast in cy46t1
    
    
    Returns
    -------
    rundates: list of datetime.datetime
        Run dates (not validity date) within the requested period
    
    observed_count: list of int
        Number of files existing on LUSTRE for each run date
    
    expected_count: list of int
        Number of files that should exist for each run date. If the observed count
        does not reach the expected_count, some files are missing.
    
    
    Notes
    -----
    TODO: change leadtime identification
    Last doctring review: 17 Mar. 2022 (TR)
    """
    
    rundates = rfs.get_rundates_between_dates(startdate, enddate)
    
    n_files_per_rundate = len(ldtdex)*len(members)
    
    expected_count = []
    observed_count = []
    
    for rundate in rundates:
        fileexists_per_rundate = 0
        for mb in members:
            for ldt in ldtdex:
                path2file = get_lustrepath(
                    domain=domain,
                    rundate=rundate,
                    member=mb,
                    leadtime=ldt,
                    prefixdir=prefixdir,
                    fileformat=fileformat,
                )
                fileexists_per_rundate += os.path.isfile(path2file)
        
        observed_count.append(fileexists_per_rundate)
        expected_count.append(n_files_per_rundate)
    
    return rundates, observed_count, expected_count

def from_files_to_arrays(
    startdate,
    enddate,
    fieldname,
    leadtimes="all",
    network="both",
    domain="glob025",
    prefixdir="test",
    fileformat="nc",
):
    """Access fields stored in files and convert them to Numpy arrays.
    
    
    Parameters
    ----------
    startdate: datetime.datetime
        Starting date of the time period to be checked. It
        corresponds to the run date (not the validity date).
    
    enddate: datetime.datetime
        Ending date of the time period to be checked. It
        corresponds to the run date (not the validity date).
    
    fieldname: str
        Short name of the requested field. Must be known in settings.py
    
    leadtimes: array-like of int or `datetime.timedelta`
        List of lead times (put "all" to get them all). Lead times (elements of
        the list) must be expressed either in hours after base time (datetime.timedelta),
        either in index (int)
    
    network: {"06", "18", "both"}
        Hour at which the run has been performed
    
    domain: {"glob025", "eurat01"}
        Geographical domain
    
    prefixdir: str
        Directory name to distinguish several purpose of use in LUSTRE
    
    fileformat: {"nc", "grib"}
        Format of the file. Must be either "nc" (netCDF4) or "grib" (GRIB2)
    
    
    Returns
    -------
    arrays: list of 2d-arrays
        List of 2-dimensional meteorological fields (almost) ready to use in ML algorithms
    
    df_meta: `pandas.DataFrame`
        Data frame containing metadata about the meteorological fields in `arrays`
    
    
    Notes
    -----
    Requires import to be discussed: reforecast_pearp_explorer.gribs, pandas
    Last doctring review: 17 Mar. 2022 (TR)
    """
    
    rundates = rfs.get_rundates_between_dates(startdate, enddate, network=network)
    
    if isinstance(leadtimes, str) and leadtimes == "all":
        leadtimes = rfs.leadtimes
        
    n_runs = rundates.size
    n_ldt = len(leadtimes)
    n_files = n_runs * n_members * n_ldt
    
    # Requires to import pandas !
    df_meta = pd.DataFrame(columns= ["rundate", "member", "leadtime", "validitydate"])
    arrays = []
    for i_run in range(n_runs):
        
        for i_mb in range(n_members):
            
            for i_ldt in range(n_ldt):
                
                srcpath = get_lustrepath(
                    domain=domain,
                    rundate=rundates[i_run],
                    member=i_mb,
                    leadtime=leadtimes[i_ldt],
                    prefixdir=prefixdir,
                    fileformat=fileformat,
                    basepath=localbasepath
                )
                
                if fileformat=="grib":
                    # Requires to import reforecast_pearp_explorer.gribs !
                    field = gribs.get_field(srcpath, fieldname)
                    field = field.data
                elif fileformat=="nc":
                    nco = nc.Dataset(srcpath,"r")
                    field = np.array(nco.variables[fieldname])
                else:
                    raise ValueError("Unknown file format: ",fileformat)
                
                validitydate = rundates[i_run] + rfs.leadtimes[i_ldt]
                df_meta = df_meta.append({"rundate":rundates[i_run], "member":i_mb, "leadtime":rfs.leadtimes[i_ldt], "validitydate":validitydate},ignore_index=True)
                arrays.append(field)
            
        
    return arrays, df_meta


def ftpdownload(prstgfile, prefixdir="test", tmpdir="", submit = False, cleanup = False):
    """Create a file of correspondance to prepare the download with FTP and
    possibly launch the download. It starts from a prestaging file.
    
    
    Parameters
    ----------
    prstgfile: str
        Path to the file containg the list of files to be moved from band to disk
    
    prefixdir: str
        Name of the directory on LUSTRE where the GRIB files will be stored
    
    tmpdir: str
        Path of the directory where the temporary files will be created
    
    submit: bool
        It True, the download request is submitted
    
    cleanup: bool
        If True, the file created by the function are deleted at the end of the function
    
    
    Returns
    -------
    ftgetfile: str
        Path to the file containing the list of transfers to be done
    
    
    Notes
    -----
    Last doctring review: 17 Mar. 2022 (TR)
    """
    
    ftgetfile = os.path.join(
        tmpdir,
        "ftget" + prstgfile[prstgfile.index("_"):]
    )
    
    f_in = open(prstgfile,'r')
    f_out = open(ftgetfile,'w')
    
    f_in.readline()  # First line is the email
    line = f_in.readline()
    
    dl_count = 0
    while len(line)>0:
        
        remotepath = line.strip('\n')
        localpath = vortexpath_to_lustrepath(remotepath, prefixdir=prefixdir)
        check_lustrepath(localpath, mkdir = True)
        
        f_out.write(remotepath + " " + localpath + "\n")
        
        line = f_in.readline()
        dl_count += 1
    
    print("\n   -***- " + str(dl_count) + " files to download! -***-\n")
    f_in.close()
    f_out.close()
    
    if submit:
        cmd_ftget = "ftget -h hendrix -u rieutordt -i "+ftgetfile
        os.system(cmd_ftget)
        print("\nAll FTP downloads done. " + dt.datetime.now().strftime("%c"))
    else:
        print("FTP downloads not submitted")
    
    if cleanup:
        cmd_cleanup = "/bin/rm -rf " + prstgfile + " " + ftgetfile
        print("\nCleaning up: " + cmd_cleanup)
        os.system(cmd_cleanup)
    
    return ftgetfile


def get_domain_from_filename(filename):
    """Extract the name of the domain from a file name as they are in output of
    ARPEGE.
    
    
    Parameters
    ----------
    filename: str
        Path of file name of the form `grid.arpege-forecast.eurat01+0030:00.grib`
        The domain to be extracted must be between "+" and "."
    
    
    Returns
    -------
    domain: str
        Geographical domain
    
    
    Notes
    -----
    Last doctring review: 17 Mar. 2022 (TR)
    """
    
    _, fn = os.path.split(filename)
    domain = fn.split("+")[0].split(".")[-1]
    
    return domain


def get_lustrepath(domain, rundate, member=0, leadtime=0, prefixdir="test", fileformat="grib", basepath=localbasepath):
    """Create the correct path to access  the grib files on LUSTRE
    
    
    Parameters
    ----------
    
    domain: {"eurat01", "glob025"}
        Geographical domain
    
    rundate: datetime.datetime
        Date and time at which the forecast is issued.
    
    member: int
        Member number
    
    leadtime: int or datetime.datetime
        Lead time, either in hours after base time (datetime.timedelta), either
        in index of outputs (int)
    
    prefixdir: str
        Directory name to distinguish several purpose of use in LUSTRE
    
    fileformat: {"nc", "grib"}
        Format of the file. Must be either "nc" (netCDF4) or "grib" (GRIB2)
    
    basepath: str
        Path to the root directory of the data
    
    Returns
    -------
    gribpath: str
        Path to the grib file
    
    
    Notes
    -----
    Last doctring review: 17 Mar. 2022 (TR)
    """
    
    filerootname = rfs.filerootname
    
    if isinstance(rundate, dt.datetime):
        rundatestr = rundate.strftime("%Y%m%dT%H%MP")
    else:
        rundatestr = str(rundate)
    
    
    if isinstance(leadtime, dt.timedelta):
        ldt = int(leadtime.total_seconds()/3600)
    else:
        ldt = int(rfs.leadtimes[leadtime].total_seconds()/3600)
    
    mbdir = "mb" + str(member).zfill(3)
    
    filename = filerootname + domain + "+" + str(ldt).zfill(4) + ":00." + fileformat
    
    lustrepath = os.path.join(
        basepath,
        prefixdir,
        rundatestr,
        mbdir,
        filename
    )
    
    return lustrepath


def id_generator(size = 6, chars = string.ascii_lowercase + string.digits):
    """Generate random strings of characters and digits than can be used as
    unique identifier
    
    
    Parameters
    ----------
    size: int
        Length of the returned string of characters
    
    chars: list
        Admissible characters. Default are lower-case alphanumeric ASCII characters
    
    
    Returns
    -------
    idstr: str
        String of `size` characters randomly taken among `chars`
    
    
    Notes
    -----
    Last doctring review: 17 Mar. 2022 (TR)
    """
    
    idstr = "".join([random.choice(chars) for _ in range(size)])
    
    return idstr


def lustrepath_to_vortexpath(lustrepath, withfilename=True):
    """
    Read the information one can find in the path to a file produced with a Vortex experiment
    
    
    Parameters
    ----------
    lustrepath: str
        Path to the file in LUSTRE (to be downloaded)
        Example: '/cnrm/recyf/NO_SAVE/Data/users/rieutordt/reforecast-pearp-cy46t1/smallsample/20180304T0600P/mb003/grid.arpege-forecast.eurat01+0006:00.grib'
    
    withfilename: bool
        If True, the output path will include the file name. If False, it will not
    
    Returns
    -------
    vtxpath: str
        Path to a file produced with a Vortex experiment
        Example: '/home/rieutordt/vortex/fullpos/ecmwf2mf/GDJ1/20180304T0600P/mb003/forecast/grid.arpege-forecast.eurat01+0006:00.grib'
    
    
    Notes
    -----
    Last doctring review: 17 Mar. 2022 (TR)
    """
    head, tail = os.path.split(lustrepath)
    pathpieces = []
    while len(tail)>0:
        pathpieces.append(tail)
        head, tail = os.path.split(head)
    
    _, _, _, _, _, username, _, _, rundatestr, mbdir, filename = pathpieces[::-1]
    
    if not withfilename:
        filename = ""
    
    hendrixbasepath = "/home/" + username + "/vortex/"
    
    vtxinfo = rfs.vtxinfo
    
    rundate = dt.datetime.strptime(rundatestr,"%Y%m%dT%H%MP")
    xpid = rfs.get_xpid_from_rundate(rundate)
    
    vortexpath = os.path.join(
        hendrixbasepath,
        vtxinfo["vapp"],
        vtxinfo["vconf"],
        xpid,
        rundatestr,
        mbdir,
        vtxinfo["block"],
        filename
    )
    
    return vortexpath


def prepare_redownload(
    startdate,
    enddate,
    domain,
    members=np.arange(n_members),
    ldtdex=np.arange(len(leadtimes)),
    network="both",
    tmpdir="",
):
    """Find all the files that are still to be downloaded on LUSTRE and create a
    file ready to be used with `ftget`.
    
    
    Parameters
    ----------
    startdate: datetime.datetime
        Starting date of the time period to be checked. It
        corresponds to the run date (not the validity date).
    
    enddate: datetime.datetime
        Ending date of the time period to be checked. It
        corresponds to the run date (not the validity date).
    
    domain: {"eurat01", "glob025"}
        Geographical domain
    
    members: array-like of int
        List of members to check. For reanalysis, this parameter is not used.
    
    ldtdex: array-like of int
        List of lead time indices.
    
    network: {"06", "18", "both"}
        Hour at which the run has been performed
    
    tmpdir: str
        Path to the directory in which the output file will be stored
    
    
    Returns
    -------
    redownload_file: str
        Path to the file that contains all the missing files on LUSTRE, ready to
        be used with `ftget`
    
    
    Notes
    -----
    TODO: change leadtime identification
    Last doctring review: 17 Mar. 2022 (TR)
    """
    
    rundates = rfs.get_rundates_between_dates(startdate, enddate, network=network)
    
    n_files_per_rundate = len(ldtdex)*len(members)
    
    uid_rdl = id_generator() # Unique ID to avoid overwritting files
    redownload_file = "_".join(
        [
            "redownload",
            username,
            rundates[0].strftime("%Y%m") + '-' + rundates[-1].strftime("%Y%m"),
            uid_rdl
        ]
    ) + ".txt"
    
    f = open(os.path.join(tmpdir, redownload_file), "w")
    print("Missing files currently written in " + f.name + "...")
    
    ctmf = 0    # Counter of missing files
    
    for rundate in rundates:
        fileexists_per_rundate = 0
        for mb in members:
            for ldt in ldtdex:
                path2file = get_lustrepath(
                    domain=domain,
                    rundate=rundate,
                    member=mb,
                    leadtime=ldt
                )
                if not os.path.isfile(path2file):
                    f.write(lustrepath_to_vortexpath(path2file) + " " + path2file + '\n')
                    ctmf += 1
                    
        
    f.close()
    print(ctmf, "missing files. Launch  `ftget -h hendrix -u "+username+" -i " + f.name + "` to re-try the download")
    
    return redownload_file


def prestage(
    # rundate_start,
    # rundate_end,
    YYMMDDHH,
    leadtimes = "all",
    network="both",
    domain="glob025",
    submit = False,
    doublecheck=True,
    cleanup=False,
    tmpdir="",
    maxfileslimit = 250000
):
    """Create a prestaging file and, if submit=True, submit the request.
    
    Prestaging consists in moving the files that will be used from band to disk.
    Indeed, the access time on disk is much lower than the one on band. Prestaging
    thus saves a lot of time by anticipating the use of the files. The request of
    prestaging is simply a text file put at a specific location. The test file
    must contain all the path and files to move on disk.
    
    
    Parameters
    ----------
    rundate_start: datetime.datetime
        First date requested. This date correspond to the date of run. The date
        of validity is obtained by adding the lead time.
    
    rundate_end: datetime.datetime
        Last date requested. This date correspond to the date of run.
    
    leadtimes: array-like of int or `datetime.timedelta`
        List of lead times (put "all" to get them all). Lead times (elements of
        the list) must be expressed either in hours after base time (datetime.timedelta),
        either in index (int)
    
    network: {"06", "18", "both"}
        Hour at which the run has been performed
    
    domain: str
        Geographical domain to be extracted. It must be taken among {'eurat01', 'glob025'}
    
    submit: bool
        It True, the prestaging request is submitted
    
    doublecheck: bool
        If True, the function starts an interactive session to verify that
        everything is good before submitting the request.
    
    cleanup: bool
        If True, the file created by the function are deleted at the end of the function
    
    tmpdir: str
        Path of the directory where the temporary files will be created
    
    maxfileslimit: int, default=250000
        Upper limit for the number of files included in a single prestaging request
    
    
    Returns
    -------
    prestaging_file: str
        Path to the file containg the list of files to be moved from band to disk
    
    
    Examples
    --------
    >>> # Prestaging of the analysis from ERA5 for the dates between 2018/10/06 and 2018/10/12
    >>> import datetime as dt
    >>> from reforecast_pearp_explorer import hendrix
    >>> dstart = dt.datetime(2018,10,6,18)
    >>> dend = dt.datetime(2018,10,12,6)
    >>> hendrix.prestage(dstart, dend, domain='eurat01', submit = False, doublecheck=True)
Vortex 1.8.0 loaded ( Thursday 02. September 2021, at 17:24:20 )

   DOUBLE CHECK
--------------------------------------------------------
  >  head -n 5 transfert2hendrix.sh
racine1=/DemandeMig/ChargeEnEspaceRapide/
ftput -h hendrix -u rieutordt -t3 -w5 prestaging_G7Q8_rieutordt_0.txt /DemandeMig/ChargeEnEspaceRapide/prestaging_G7Q8_rieutordt_0.MIG
dirh -d /DemandeMig/ChargeEnEspaceRapide/

--------------------------------------------------------
  >  head -n 10 prestaging_G7Q8_rieutordt_0.txt
#MAIL=thomas.rieutord@meteo.fr
/home/rieutordt/vortex/fullpos/ecmwf2mf/G7Q8/20181006T1800P/fullpos/cpl.ifs-determ-prod.tl1198-c22+0000:00.fa
/home/rieutordt/vortex/fullpos/ecmwf2mf/G7Q8/20181007T1800P/fullpos/cpl.ifs-determ-prod.tl1198-c22+0000:00.fa
/home/rieutordt/vortex/fullpos/ecmwf2mf/G7Q8/20181008T1800P/fullpos/cpl.ifs-determ-prod.tl1198-c22+0000:00.fa
/home/rieutordt/vortex/fullpos/ecmwf2mf/G7Q8/20181009T1800P/fullpos/cpl.ifs-determ-prod.tl1198-c22+0000:00.fa
/home/rieutordt/vortex/fullpos/ecmwf2mf/G7Q8/20181010T1800P/fullpos/cpl.ifs-determ-prod.tl1198-c22+0000:00.fa
/home/rieutordt/vortex/fullpos/ecmwf2mf/G7Q8/20181011T1800P/fullpos/cpl.ifs-determ-prod.tl1198-c22+0000:00.fa

--------------------------------------------------------
submit = False cleanup = False tmpdir= 

  Do you wish to abort the execution?  (yes/no)no

Transfert script has not been executed
dirh -d /DemandeMig/ChargeEnEspaceRapide/
    
    
    Notes
    -----
    TODO: update example
    TODO: uniformize variable names "startdate"/"rundate_start"...
    TODO: check for extra "_0" in the prestaging file name
    Last doctring review: 17 Mar. 2022 (TR)
    """
    hendrix_prestage_dir = "/DemandeMig/ChargeEnEspaceRapide/"
    hendrixbasepath = "/home/" + username + "/vortex/"
    transfert_script = "transfert2hendrix.sh"
    
    vtxinfo = rfs.vtxinfo
    
    # YYMMDDHH = rfs.get_rundates_between_dates(rundate_start,rundate_end, network="both")
    n_runs = YYMMDDHH.size
    
    if isinstance(leadtimes, str) and leadtimes == "all":
        leadtimes = rfs.leadtimes
    
    n_ldt = len(leadtimes)
    
    n_files = n_runs * n_members * n_ldt
    
    n_prestaging = n_files // maxfileslimit + 1
    
    if n_prestaging > 1:
        print(
            "WARNING: the number of files exceeds the limit. The request will be divided in",
            n_prestaging,
            "different requests"
        )
    
    i_fb2d = 1      # Counter of files to be moved from band to disk (fb2d = file band to disk)
    i_prstg = 0     # Counter of prestaging requests (prstg = prestaging)
    uid_prstg = id_generator() # Unique ID to avoid overwritting prestaging files
    
    prestaging_rootfilename = "_".join( 
        [
            "prestaging",
            username,
            YYMMDDHH[0].strftime("%Y%m") + '-' + YYMMDDHH[-1].strftime("%Y%m"),
            uid_prstg,
        ]
    ) + "_"
    prestaging_file = prestaging_rootfilename + str(i_prstg) + ".txt"
    
    f = open(os.path.join(tmpdir, prestaging_file), "w")
    # Email on the first line (to receive notifications)
    f.write("#MAIL=" + useremail + "\n")
    
    for i_run in range(n_runs):
        rundate = YYMMDDHH[i_run]
        xpid = rfs.get_xpid_from_rundate(rundate)
        
        for i_mb in range(n_members):
            mbdir = "mb" + str(i_mb).zfill(3)
            vortexpath = os.path.join(
                hendrixbasepath,
                vtxinfo["vapp"],
                vtxinfo["vconf"],
                xpid,
                rundate.strftime("%Y%m%dT%H%MP"),
                mbdir,
                vtxinfo["block"]
            )
            
            for i_ldt in range(n_ldt):
                if i_fb2d >= maxfileslimit - 1:
                    f.close()
                    i_prstg += 1
                    prestaging_file = prestaging_rootfilename + str(i_prstg) + ".txt"
                    
                    f = open(tmpdir + prestaging_file, "w")
                    
                    # Email on the first line (to receive notifications)
                    f.write("#MAIL=" + mail + "\n")
                    i_fb2d = 1
                
                ldt = leadtimes[i_ldt]
                if isinstance(ldt, dt.timedelta):
                    sldt = str(int(ldt.total_seconds()/3600)).zfill(4)
                else:
                    sldt = str(int(rfs.leadtimes[ldt].total_seconds()/3600)).zfill(4)
                
                filename_fb2d = (
                    vtxinfo["filerootname"]
                    + domain+"+"
                    + sldt
                    + ":00."
                    + vtxinfo["fileformat"]
                )
                
                f.write( os.path.join(vortexpath, filename_fb2d) + "\n")
                i_fb2d += 1
    
    f.close()
    
    f2 = open(os.path.join(tmpdir, transfert_script), "w")
    f2.write("racine1=" + hendrix_prestage_dir + "\n")
    for i_pre in range(n_prestaging):
        prestaging_localfile = prestaging_rootfilename + str(i_pre) + ".txt"
        prestaging_remotefile = os.path.join(
            hendrix_prestage_dir,
            prestaging_rootfilename + str(i_pre) + ".MIG"
        )
        
        f2.write(
            "ftput -h hendrix -u " + username + " -t3 -w5 "
            + os.path.join(tmpdir,prestaging_localfile)
            + " "
            + prestaging_remotefile
            + "\n"
        )
    
    f2.close()
    
    # Authorize execution
    os.system("chmod +x " + os.path.join(tmpdir, transfert_script))
    
    if doublecheck:
        print("\n   DOUBLE CHECK")
        print("--------------------------------------------------------")
        cmd_chk_script = "head -n 5 " + os.path.join(tmpdir, transfert_script)
        print("  >  " + cmd_chk_script)
        os.system(cmd_chk_script)
        print("\n--------------------------------------------------------")
        cmd_chk_file = "head -n 10 " + os.path.join(tmpdir, prestaging_file)
        print("  >  " + cmd_chk_file)
        os.system(cmd_chk_file)
        print("\n--------------------------------------------------------")
        print("submit =", submit, "; cleanup =", cleanup, "; tmpdir =", tmpdir)
        answer = input("\n  Continue?  (yes/no)")
        if answer.lower() in ['n','no','non']:
            exit("Execution stopped by the user")
    
    # Transfer file
    if submit:
        print("\nExecution of the transfert script")
        os.system(os.path.join(tmpdir, transfert_script))
    else:
        print("\nTransfert script has not been executed")
    
    cmd_chk_dirh = "dirh -d " + hendrix_prestage_dir
    print(cmd_chk_dirh)
    os.system(cmd_chk_dirh)
    print("Your last file:" + prestaging_file)
    
    # Cleaning
    if cleanup:
        print("\nCleaning up...")
        os.system(
            "/bin/rm -rf "
            + os.path.join(tmpdir, prestaging_file)
        )
        os.system("/bin/rm -rf " + os.path.join(tmpdir, transfert_script))
    
    return os.path.join(tmpdir, prestaging_file)


def upload_to_ftpserver(
    startdate,
    enddate,
    leadtimes="all",
    network="both",
    domain="glob025",
    prefixdir="test",
    fileformat="nc",
    submit = False,
    doublecheck = True,
    ftp_host="ftp.umr-cnrm.fr",
    ftp_host_basepath="/reforecast-cy46t1",
    ftp_login="eunadics",
    ftp_pwd = "18pl@smA20",
    approx_file_size = 25,
    maxvolume = 200000,
):
    """Upload files onto a FTP server
    
    
    Parameters
    ----------
    startdate: datetime.datetime
        First date requested. This date correspond to the date of run. The date
        of validity is obtained by adding the lead time.
    
    enddate: datetime.datetime
        Last date requested. This date correspond to the date of run.
    
    leadtimes: array-like of int or `datetime.timedelta`
        List of lead times (put "all" to get them all). Lead times (elements of
        the list) must be expressed either in hours after base time (datetime.timedelta),
        either in index (int)
    
    network: {"06", "18", "both"}
        Hour at which the run has been performed
    
    domain: str
        Geographical domain to be extracted. It must be taken among {'eurat01', 'glob025'}
    
    prefixdir: str
        Directory name to distinguish several purpose of use in LUSTRE
    
    fileformat: {"nc", "grib"}
        Format of the file. Must be either "nc" (netCDF4) or "grib" (GRIB2)
    
    submit: bool
        It True, the uploading request is submitted
    
    doublecheck: bool
        If True, the function starts an interactive session to verify that
        everything is good before submitting the request.
    
    ftp_host: str
        URL of the remote FTP server
    
    ftp_host_basepath: str
        Path to the root directory of the data on the FTP server
    
    ftp_login: str
        Login to connect to the FTP server
    
    ftp_pwd: str
        Password to connect to the FTP server
    
    approx_file_size: int, default=160
        Approximate volume of a single file (in Mo)
        
    maxvolume: int, default=200000
        Upper limit for the volume to upload on the FTP server (in Mo)
    
    
    Returns
    -------
    cmd_history: list of str
        List of shell command that were executed if submit=True
    
    
    See also
    --------
    LFTP documentation (2022/03/14): http://lftp.yar.ru/lftp-man.html
    
    
    Notes
    -----
    Last doctring review: 17 Mar. 2022 (TR)
    """
    cwd = os.getcwd()
    
    rundates = rfs.get_rundates_between_dates(startdate, enddate, network=network)
    
    if isinstance(leadtimes, str) and leadtimes == "all":
        leadtimes = rfs.leadtimes
        
    n_runs = rundates.size
    n_ldt = len(leadtimes)
    n_files = n_runs * n_members * n_ldt
    
    if approx_file_size*n_files > maxvolume:
        raise Exception("Too large volume to be sent:", np.round(approx_file_size*n_files/1024,1), "Go")
    
    cmd_history = []
    
    os.chdir(localbasepath)
    
    for i_run in range(n_runs):
        
        for i_mb in range(n_members):
            
            for i_ldt in range(n_ldt):
                
                srcpath = get_lustrepath(
                    domain=domain,
                    rundate=rundates[i_run],
                    member=i_mb,
                    leadtime=leadtimes[i_ldt],
                    prefixdir=prefixdir,
                    fileformat=fileformat,
                    basepath=""
                )
                
                lftp_cmd = "lftp -u "+ftp_login+","+ftp_pwd+' -e "mput -d '+srcpath+' -O '+ftp_host_basepath+';quit" ftp://'+ftp_host
                
                cmd_history.append(lftp_cmd)
            
        
    if doublecheck:
        print("Current directory is ", os.getcwd())
        print("Here are 5 randomly chosen requests:")
        print("\n".join(np.random.choice(cmd_history,5)))
        print(len(cmd_history),"files to upload.", np.round(approx_file_size*n_files/1024,1), "Go. submit=",submit)
        answer = input("Continue ? (yes/no)")
        if answer.lower() in ['n','no','non']:
            exit("Execution stopped by the user")
        
    
    if submit:
        for lftp_cmd in cmd_history:
            os.system(lftp_cmd)
    else:
        print("LFTP requests not submitted because submit=",submit)
    
    os.chdir(cwd)
    return cmd_history


def vortexpath_to_lustrepath(vtxpath, prefixdir="test", withfilename=True):
    """
    Read the information one can find in the path to a file produced with a Vortex experiment
    
    
    Parameters
    ----------
    vtxpath: str
        Path to a file produced with a Vortex experiment
        Example: '/home/rieutordt/vortex/fullpos/ecmwf2mf/G7Q8/20181006T1800P/fullpos/cpl.ifs-determ-prod.tl1198-c22+0000:00.fa'
    
    prefixdir: str
        Directory name to distinguish several purpose of use in LUSTRE.
    
    withfilename: bool
        If True, the output path will include the file name. If False, it will not
    
    
    Returns
    -------
    lustrepath: str
        Path to the file in LUSTRE (to be downloaded)
    
    
    Notes
    -----
    TODO: update example
    Last doctring review: 17 Mar. 2022 (TR)
    """
    head, tail = os.path.split(vtxpath)
    pathpieces = []
    while len(tail)>0:
        pathpieces.append(tail)
        head, tail = os.path.split(head)
    
    _, _, _, vapp, vconf, xpid, rundatestr, mbdir, block, filename = pathpieces[::-1]
    
    if not withfilename:
        filename = ""
    
    lustrepath = os.path.join(
        localbasepath,
        prefixdir,
        rundatestr,
        mbdir,
        filename
    )
    
    return lustrepath

def get_rhandlers(t,rundates, leadtimes = "all", members = "all", domains = ["eurat01", "glob025"], quiet=False, toolbar_width = 30):
    """Create the resource handler list for VORTEX
    """
    
    if isinstance(leadtimes, str) and leadtimes=="all":
        leadtimes = rfs.leadtimes
    if isinstance(members, str) and members=="all":
        members = [i for i in range(rfs.n_members)]
    
    vortex.setup.defaults = dict(
        cutoff = 'production',
        kind='gridpoint',
        origin='historic',
        model = 'arpege',
        vapp = 'arpege',
        vconf = 'pearp',
        block='forecast',
        namespace='vortex.multi.fr',
    )
    
    n_runs = rundates.size
    n_ldt = len(leadtimes)
    n_members = len(members)
    n_files = n_runs * n_members * n_ldt
    
    rhandlers = []
    
    # setup toolbar
    if not quiet:
        t0 = time.time()  #::::::::::::::::::::::
        
        toolbar_batch = int(n_runs/toolbar_width)
        
        if toolbar_batch==0:
            toolbar_width = n_runs
            toolbar_batch = 1
        
        sys.stdout.write(
            "Creating the rhandlers list [%s]" % ("." * toolbar_width)
        )
        sys.stdout.flush()
        sys.stdout.write("\b" * (toolbar_width + 1))  # return to start of line, after '['
    
    for i_run in range(n_runs):
        
        # core toolbar
        if not quiet:
            if i_run % toolbar_batch == 0:
                sys.stdout.write("*")
                sys.stdout.flush()
        
        rundate = rundates[i_run]
        xpid = rfs.get_xpid_from_rundate(rundate)
        
        for i_mb in range(n_members):
            member = members[i_mb]
            
            for i_ldt in range(n_ldt):
                leadtime=leadtimes[i_ldt]
                ldt = int(leadtime.total_seconds()/3600)
                
                rh = vortex.toolbox.rload(
                    # Resource
                    geometry=domains,
                    date = Date(rundate.strftime("%Y%m%d%H")),
                    term = ldt,
                    nativefmt='[format]',
                    # Provider
                    experiment=xpid,
                    member=member,
                    # Container
                    filename='[model]_[date::ymdh]_mb[member].[geometry::area]+[term::fmthm].[format]',
                    format="grib",
                )
                
                rhandlers += rh
            
        
    # end toolbar
    if not quiet:
        t1 = time.time()  #::::::::::::::::::::::
        chrono = int((t1 - t0)*100)/100.0
        sys.stdout.write("] (" + str(chrono) + " s)\n")
    
    return rhandlers



# End of file
