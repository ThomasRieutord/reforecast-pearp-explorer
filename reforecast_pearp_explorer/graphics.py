#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Tools to access the long-term PEARP reforecast made at CNRM/GMAP/RECYF.
Graphical functions

@author: Rieutord Thomas
@date: 2021/10/20

Global docstrings reviews:
  * 01 Mar. 2022 by Thomas Rieutord (TR)
  * 17 Mar. 2022 by Thomas Rieutord (TR)
"""

import os.path
import epygram
import datetime as dt
import pandas as pd
import matplotlib.pyplot as plt

# Local packages
from reforecast_pearp_explorer import access
from reforecast_pearp_explorer.settings import ReforecastSettings


fmtImages = ".png"
# Images will be saved under this format (suffix of plt.savefig)

figureDir = "../figures"
# Images will be saved in this directory (prefix of plt.savefig)

storeImages = False
# If True, figures are saved in files but not shown
# If False, figures are not saved in files but always shown



rfs = ReforecastSettings().build()

localbasepath = rfs.localbasepath

n_members = rfs.n_members
leadtimes = rfs.leadtimes


def barplot_fileexist(
    rundates,
    observed_count,
    expected_count,
    groupby="month",
    title=None,
    figname = "barplot_fileexist"
):
    """Barplot to check the number of file that exist in a given range of dates
    
    
    Parameters
    ----------
    rundates: list of datetime.datetime
        Run dates (not validity date) within the requested period
    
    observed_count: list of int
        Number of files existing on LUSTRE for each run date
    
    expected_count: list of int
        Number of files that should exist for each run date. If the observed count
        does not reach the expected_count, some files are missing.
    
    groupby: {"year", "month", "day"}
        Time unit for the grouping. Default is monthly.
    
    title: str
        Title of the figure. If not provided, a default one will be given.
    
    figname: str
        File name for the saved figure. If not provided, a default one will be given.
    
    
    Returns
    -------
    None. The figure is displayed or stored.
    
    
    Notes
    -----
    Last doctring review: 17 Mar. 2022 (TR)
    """
    
    df = pd.DataFrame(index=rundates, data={"obscount":observed_count, "expcount":expected_count})
    
    try:
        gdf = df.groupby(eval("df.index."+groupby)).sum()
    except AttributeError:
        raise ValueError("The groupby argument " + groupby + "is invalid")
    
    x = gdf.index
    yo = gdf.obscount
    ye = gdf.expcount
    
    if title is None:
        title = "Number of existing files grouped by "+ groupby +" between " + rundates[0].strftime("%x") + " and " + rundates[-1].strftime("%x")
    
    plt.figure()
    plt.title(title)
    plt.grid()
    plt.plot(x, ye, "k--+", label="expected")
    plt.bar(x, yo, label="observed")
    plt.legend(loc='best')
    if storeImages:
        figpath = os.path.join(figureDir, figname + fmtImages)
        plt.savefig(figpath)
        plt.close()
        print("Figure saved:", figpath)
    else:
        plt.show(block=False)
    

def plot_field(
    epyfield,
    ech=0,
    mb=0,
    figname = None,
):
    """Simple display of a field on a map.
    
    
    Parameters
    ----------
    epyfield: `epygram.fields`
        Meteorological field to display
    
    ech: int
        Lead time
    
    mb: int
        Member number
    
    figname: str
        File name in which the figure will be stored
    
    
    Returns
    -------
    None. The figure is displayed or stored.
    
    
    Notes
    -----
    Last doctring review: 17 Mar. 2022 (TR)
    """
    
    
    try:
        lvl = "at level" + str(epyfield.fid['GRIB2']['level'])
        slvl = str(epyfield.fid['GRIB2']['level'])
    except KeyError:
        lvl = ""
        slvl = ''
    
    figtitle = " ".join(
        [
            epyfield.fid['GRIB2']['name'],
            lvl,
            "valid at",
            epyfield.validity.get().strftime("%Y-%m-%d %H:%M"),
            "(+"+str(int(epyfield.validity.term().total_seconds()/3600))+"h)",
            "mb" + str(mb).zfill(3)
        ]
    )
    
    if epyfield.geometry.isglobal:
        domain = "glob025"
    else:
        domain = "eurat01"
    
    if figname is None:
        figname = "_".join(
            [
                "plotfield",
                epyfield.fid['GRIB2']['shortName'],
                slvl,
                domain,
            ]
        )
    
    epyfield.cartoplot(title = figtitle)
    
    if storeImages:
        figpath = os.path.join(figureDir, figname + fmtImages)
        plt.savefig(figpath)
        plt.close()
        print("Figure saved:", figpath)
    else:
        plt.show(block=False)
    

# End of file
