#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Tools to access the long-term PEARP reforecast made at CNRM/GMAP/RECYF.
Module to store object-oriented information about the reforecast

@author: Rieutord Thomas (thomas.rieutord@meteo.fr)
@institution: Météo-France DESR/CNRM/GMAP/RECYF
@creationdate: 2022/02/25

For the GRIB files, useful resources are
  * ECMWF GRIB database : https://apps.ecmwf.int/codes/grib/param-db/
  * GMAP tool What's The Grib (accessible inside MF network) : http://intra.cnrm.meteo.fr/gws/wtg/
  * CF conventions standard names table : http://cfconventions.org/Data/cf-standard-names/current/build/cf-standard-name-table.html

For the reforecast data organization, please ask the author.


Global docstrings reviews:
  * 04 Mar. 2022 by Thomas Rieutord (TR)
  * 17 Mar. 2022 by Thomas Rieutord (TR)
"""

import os
import datetime as dt
import numpy as np

class ReforecastSettings():
    """
    Reforecast settings
    """
    
    def __init__(self, cycle="46t1"):
        """Constructor of ReforecastSettings
        
        Contain all the data to know about the reforecast settings.
        """
        # General
        self.cycle = cycle
        self.startdate = dt.datetime(2018,10,8,18)
        self.enddate = dt.datetime(2018,10,28,18)
        self.time_between_runs = dt.timedelta(hours = 60)
        self.maxleadtime = 108
        self.leadtimes = np.arange(0, self.maxleadtime+3, 3)*dt.timedelta(hours=1)
        self.n_members = 11
        
        # GRIB-related
        self.all_2d_fieldnames = [
            "wind_speed_at_10_meters",
            "wind_speed_of_gust_at_10_meters",
            "air_temperature_at_2_meters",
            "air_pressure_at_mean_sea_level",
            "precipitation_amount",
            "snowfall_amount",
            "geopotential_height_at_1.5_PVU",
            "height_at_0_wet_bulb_potential_temperature",
            "height_at_1_wet_bulb_potential_temperature",
            "x_wind_gust_at_10_meters",
            "y_wind_gust_at_10_meters",
            "eastward_wind_at_10_meters",
            "northward_wind_at_10_meters",
            "eastward_wind_at_100_meters",
            "northward_wind_at_100_meters",
            "cape_instantaneous",
            "wet_bulb_potential_temperature_at_850_hPa",
        ]
        self.all_2d_field2gribid  = {
            # Surface
            "air_pressure_at_mean_sea_level":{'parameterNumber':1, 'shortName':'prmsl'},
            "air_temperature_at_2_meters":{'parameterNumber':0, 'shortName':'2t', "typeOfFirstFixedSurface":103,'productDefinitionTemplateNumber':1},
            "wind_speed_at_10_meters":{"calculated":True},   # sqrt(10u**2 + 10v**2)
            "wind_speed_of_gust_at_10_meters":{"calculated":True},  # sqrt(10nfg**2 + 10efg**2)
            "precipitation_amount":{"calculated":True},    # lsp + cp + lssrwe + csrwe
            "snowfall_amount":{"calculated":True},   # lssrwe + csrwe
            "geopotential_height_at_1.5_PVU":{"parameterNumber":4, "shortName":"z", "typeOfFirstFixedSurface":109, 'scaledValueOfFirstFixedSurface': 1500},
            "height_at_0_wet_bulb_potential_temperature":{"discipline":0, "parameterCategory":3, "parameterNumber":6, "typeOfFirstFixedSurface":198, "scaledValueOfFirstFixedSurface":27315},
            "height_at_1_wet_bulb_potential_temperature":{"discipline":0, "parameterCategory":3, "parameterNumber":6, "typeOfFirstFixedSurface":198, "scaledValueOfFirstFixedSurface":27415},
            "x_wind_gust_at_10_meters":{"discipline":0,"parameterCategory":2,"parameterNumber":23, "shortName":"ugust", "lengthOfTimeRange":1},
            "y_wind_gust_at_10_meters":{"discipline":0,"parameterCategory":2,"parameterNumber":24, "shortName":"vgust", "lengthOfTimeRange":1},
            "eastward_wind_at_10_meters":{"discipline":0,"parameterCategory":2,"parameterNumber":2, "shortName":"10u", "typeOfFirstFixedSurface":103},
            "northward_wind_at_10_meters":{"discipline":0,"parameterCategory":2,"parameterNumber":3, "shortName":"10v", "typeOfFirstFixedSurface":103},
            "eastward_wind_at_100_meters":{"discipline":0,"parameterCategory":2,"parameterNumber":2, "typeOfFirstFixedSurface":103, "level":100},
            "northward_wind_at_100_meters":{"discipline":0,"parameterCategory":2,"parameterNumber":3, "typeOfFirstFixedSurface":103, "level":100},
            "cape_instantaneous":{"discipline":0, "parameterCategory":7, "parameterNumber":6, "shortName":"CAPE_INS"},
            "wet_bulb_potential_temperature_at_850_hPa":{"discipline":0, "parameterCategory":0, "parameterNumber":3, "typeOfFirstFixedSurface":100, "level":850},
            "surface_upward_latent_heat_flux":{"discipline":0, "parameterCategory":0, "parameterNumber":10, "typeOfFirstFixedSurface":1},
            "surface_upward_sensible_heat_flux":{"discipline":0, "parameterCategory":0, "parameterNumber":11, "typeOfFirstFixedSurface":1},
            "total_convective_precipitation_amount":{"calculated":True},    # cp + csrwe
            "total_cloud_cover":{"discipline":0,"parameterCategory":6,"parameterNumber":1},
        }
        
        self.all_3d_fieldnames = [
            "eastward_wind",
            "northward_wind",
            "air_temperature",
            "geopotential",
            "relative_humidity",
            "air_potential_temperature",
            "potential_vorticity",
            "absolute_vorticity",
            "wet_bulb_temperature",
            "divergence_of_wind",
            "upward_air_velocity",
        ]
        self.all_3d_field2gribid = {
            "eastward_wind":{'parameterNumber':2, "shortName":"u", "typeOfFirstFixedSurface":100},
            "northward_wind":{'parameterNumber':3, "shortName":"v", "typeOfFirstFixedSurface":100},
            "air_temperature":{'parameterNumber':0, "shortName":"t"},
            "geopotential":{'parameterNumber':4, "shortName":"z"},
            "relative_humidity":{'parameterNumber':1, "shortName":"r"},
            "air_potential_temperature":{"calculated":True},
            "potential_vorticity":{'parameterNumber':14, "shortName":"pv"},
            "absolute_vorticity":{"discipline":0, "parameterCategory":2, "parameterNumber":10, "shortName":"absv"},
            "relative_vorticity":{'discipline':255,'parameterCategory':255,'parameterNumber':255, "typeOfFirstFixedSurface":100},
            "wet_bulb_temperature":{"discipline":0, "parameterCategory":0, "parameterNumber":27},
            "divergence_of_wind":{"discipline":0, "parameterCategory":2, "parameterNumber":13},
            "upward_air_velocity":{"discipline":0, "parameterCategory":2, "parameterNumber":8},
        }
        self.all_pressure_levels = [1000, 950, 850, 700, 600, 500, 400, 300, 250]
        self.units_of_vertical_levels = {
            "hPa":{"typeOfFirstFixedSurface":100},
            "meters":{"typeOfFirstFixedSurface":103}
        }
        
        
        # OLIVE-related
        self.veryfirstdate = dt.datetime(2000,3,1,18)
        self.verylastdate = dt.datetime(2022,3,1,18)
        self.n_years_per_block = 2
        self.xpids = [
            #  06H    18H
            ["GDML","GDMK"], # 2000-2002
            ["GDMJ","GDMI"], # 2002-2004
            ["GDM6","GDM7"], # 2004-2006
            ["GDM4","GDM3"], # 2006-2008
            ["GDLY","GDLX"], # 2008-2010
            ["GDLR","GDLQ"], # 2010-2012
            ["GDL1","GDL2"], # 2012-2014
            ["GDKF","GDKG"], # 2014-2016
            ["GDJ5","GDJ6"], # 2016-2018
            ["GDJ2","GDJ1"], # 2018-2020
            ["GG08","GG09"], # 2018-2020
        ]
        self.vtxinfo = {
            "vapp":"arpege",
            "vconf":"pearp",
            "block":"forecast",
            "filerootname":"grid.arpege-forecast.",
            "fileformat":"grib"
        }
        self.vapp="arpege"
        self.vconf="pearp"
        self.filerootname="grid.arpege-forecast."
        self.lustrebasepath = "/cnrm/recyf/NO_SAVE/Data/users/rieutordt/reforecast-pearp-cy46t1"
        self.belenosbasepath = "/scratch/mtool/rieutordt/cache/usertmp/reforecast-pearp-cy46t1"
        self.nuwabasepath = "/mesonh/panf/reforecast"
        self.useremail = "thomas.rieutord@meteo.fr"
        self.username = "rieutordt"
        
        self.build()
        
    
    def build(self):
        """Compute additional parameters based on the settings
        
        
        Parameters
        ----------
        self: `rpe.settings.ReforecastSettings` instance
            Object containing data about the reforecast settings
        
        Returns
        ----------
        self: `rpe.settings.ReforecastSettings` instance
            Object containing data about the reforecast settings
        
        Notes
        -----
        Last doctring review: 17 Mar. 2022 (TR)
        """
        
        self.all_fieldnames = self.all_2d_fieldnames + self.add_vlevel_to_fieldnames(
            self.all_3d_fieldnames, self.all_pressure_levels
        )
        
        self.all_field2gribid = self.add_vlevel_to_field2gribid(
           self.all_3d_field2gribid,  self.all_3d_fieldnames, self.all_pressure_levels
        )
        self.all_field2gribid.update(self.all_2d_field2gribid)
        
        
        self.n_olive_blocks = len(self.xpids)
        
        self.xpid_rundates = self.get_xpid_rundates(
            self.veryfirstdate, self.n_years_per_block, self.time_between_runs
        )
        
        self.xpid_dateboundaries = {
            xpid:[
                min(self.xpid_rundates[xpid]), max(self.xpid_rundates[xpid])
            ] for xpid in self.xpid_rundates.keys()
        }
        
        hostname = os.uname()[1]
        self.localbasepath = self.get_localbasepath(hostname)
        
        # Expressed needs (cf mail B. Doiteau 9/3/22)
        self.borders_for_extraction = {
            "glob025":{"lonmin":-75, "latmin":20, "lonmax":55, "latmax":70},
            "eurat01":{"lonmin":-20, "latmin":20, "lonmax":45, "latmax":55},
        }
        self.fields_to_extract = {
            "glob025":[
                "geopotential_height_at_1.5_PVU",
                "air_pressure_at_mean_sea_level",
            ] + self.add_vlevel_to_fieldnames(
                [
                    "eastward_wind",
                    "northward_wind",
                    "air_temperature",
                    "geopotential",
                    "relative_humidity",
                    "air_potential_temperature",
                    "potential_vorticity",
                ],
                [850, 500, 250]
            ) + self.add_vlevel_to_fieldnames(
                [
                    "absolute_vorticity",
                    "wet_bulb_temperature",
                ],
                [850]
            ) + self.add_vlevel_to_fieldnames(
                [
                    "eastward_wind",
                    "northward_wind",
                ],
                [700]
            ) + self.add_vlevel_to_fieldnames(
                ["divergence_of_wind"],
                [250,850]
            ) + self.add_vlevel_to_fieldnames(
                ["upward_air_velocity"],
                [400,600]
            ),
            "eurat01":[
                "x_wind_gust_at_10_meters",
                "y_wind_gust_at_10_meters",
                "wet_bulb_potential_temperature_at_850_hPa",
                "air_temperature_at_2_meters",
                "cape_instantaneous",
                "wind_speed_of_gust_at_10_meters",
                "air_temperature_at_2_meters",
                "precipitation_amount",
                "height_at_0_wet_bulb_potential_temperature",
                "height_at_1_wet_bulb_potential_temperature",
                "snowfall_amount",
                "surface_upward_latent_heat_flux",
                "surface_upward_sensible_heat_flux",
                "total_convective_precipitation_amount",
                "total_cloud_cover",
            ] + self.add_vlevel_to_fieldnames(
                ["air_temperature", "wet_bulb_temperature"],
                [1000, 850, 700, 500, 300]
            ) + self.add_vlevel_to_fieldnames(
                ["eastward_wind","northward_wind"],
                [10,100],
                "meters"
            ),
        }
        
        return self

    def add_vlevel_to_fieldnames(self, lfn, lvlvl, vlvl_unit = "hPa"):
        """Add the appropriate suffix the field name to precise the vertical level
        according the CF conventions.
        
        
        Parameters
        ----------
        self: `rpe.settings.ReforecastSettings` instance
            Object containing data about the reforecast settings
        
        lfn: array-like
            List of file names
        
        lvlvl: array-like
            List of vertical levels
        
        vlvl_unit: str
            Vertical levels unit
        
        
        Returns
        
        Notes
        -----
        Last doctring review: 17 Mar. 2022 (TR)
        -------
        lfnv: list
            List of field names with vertical level
        
        
        Notes
        -----
        Last doctring review: 17 Mar. 2022 (TR)
        """
        lfnv = []
        for fd in lfn:
            for vlvl in lvlvl:
                lfnv.append(fd + "_"+"_".join(["at", str(vlvl), vlvl_unit]))
        
        return lfnv
    
    def add_vlevel_to_field2gribid(self, fd2gid, lfn, lvlvl, vlvl_unit = "hPa"):
        """Add the necessary fields in the GRIB field ID (handgrip) to precise the vertical level
        
        
        Parameters
        ----------
        self: `rpe.settings.ReforecastSettings` instance
            Object containing data about the reforecast settings
        
        lfn: array-like
            List of file names
        
        fd2gid: dict
            Field GRIB ID without vertical level information
        
        lvlvl: array-like
            List of vertical levels
        
        vlvl_unit: str
            Vertical levels unit
        
        
        Returns
        -------
        fd2gidv: dict
            Field GRIB ID with vertical level information for all listed fields
        
        
        Notes
        -----
        Last doctring review: 17 Mar. 2022 (TR)
        """
        fd2gidv = {}
        
        for fd in lfn:
            hg = {**fd2gid[fd]}
            for vlvl in lvlvl:
                hg.update({"level":vlvl})
                hg.update(self.units_of_vertical_levels[vlvl_unit])
                lfnv = self.add_vlevel_to_fieldnames([fd], [vlvl], vlvl_unit)
                fd2gidv[lfnv[0]] = {**hg}
        
        return fd2gidv
        
    
    def get_localbasepath(self, hostname):
        """Return the `localbasepath` corresponding to the host.
        """
        
        if "belenos" in hostname:
            localbasepath = self.belenosbasepath
        elif "recyf" in hostname:
            localbasepath = self.lustrebasepath
        elif "phynh" in hostname:
            localbasepath = self.lustrebasepath
        elif "bigdata" in hostname:
            localbasepath = self.lustrebasepath
        elif "nuwa" in hostname:
            localbasepath = self.nuwabasepath
        else:
            raise Exception("Unknown hostname. Unable to find the base path", hostname)
        
        return localbasepath
        
    def get_2networks_dates(self, startdate, enddate, time_between_runs):
        """Get the rundates of all runs at 06UTC and 18UTC within the given boundaries
        
        
        Parameters
        ----------
        self: `rpe.settings.ReforecastSettings` instance
            Object containing data about the reforecast settings
        
        startdate: datetime.datetime
            First date requested. This date correspond to the date of run. The date
            of validity is obtained by adding the lead time.
        
        enddate: datetime.datetime
            Last date requested. This date correspond to the date of run.
        
        time_between_runs: datetime.timedelta
            Time between two successive runs (assumed to be constant)
        
        
        Returns
        -------
        l06: list
            List of dates with the 06UTC run
        
        l18: list
            List of dates with the 18UTC run
        
        
        Notes
        -----
        Last doctring review: 17 Mar. 2022 (TR)
        """
        l06=[]
        l18=[]
        d = startdate
        while d<=enddate:
            if d.hour==6:
                l06.append(d)
            elif d.hour==18:
                l18.append(d)
            d = d + time_between_runs
        
        return l06, l18
    
    def get_xpid_rundates(self, veryfirstdate, n_years_per_block, time_between_runs):
        """Get the run dates that were performed for each OLIVE experiment
        
        
        Parameters
        ----------
        self: `rpe.settings.ReforecastSettings` instance
            Object containing data about the reforecast settings
        
        veryfirstdate: datetime.datetime
            First run date of the whole reforecast
        
        n_years_per_block: int
            Number of years covered by each OLIVE block of experiments
        
        time_between_runs: datetime.timedelta
            Time between two successive runs (assumed to be constant)
        
        
        Returns
        -------
        xpid_rundates: dict
            Keys are xpids and values are lists of run dates
        
        
        Notes
        -----
        Last doctring review: 17 Mar. 2022 (TR)
        """
        xpid_rundates = {}
        n_olive_blocks = len(self.xpids)
        for i_blk in range(n_olive_blocks):
            xpid06, xpid18 = self.xpids[i_blk]
            
            firstdate = dt.datetime(
                veryfirstdate.year + i_blk*n_years_per_block,
                veryfirstdate.month,
                veryfirstdate.day,
                veryfirstdate.hour
            )
            
            lastdate = dt.datetime(
                veryfirstdate.year + (i_blk+1)*n_years_per_block,
                veryfirstdate.month,
                veryfirstdate.day,
                veryfirstdate.hour
            )
            
            rundates06, rundates18 = self.get_2networks_dates(
                firstdate, lastdate, time_between_runs
            )
            
            xpid_rundates.update({xpid06:[*rundates06], xpid18:[*rundates18]})
            
        return xpid_rundates
    
    def get_xpid_from_rundate(self, rundate):
        """Get the xpid from the run date (different from the validity date)
        
        Differ from get_xpid_from_date by its adaptation to cy46t1
        
        
        Parameters
        ----------
        self: `rpe.settings.ReforecastSettings` instance
            Object containing data about the reforecast settings
        
        rundate: datetime.datetime
            Date at which the forecast began
        
        
        Returns
        -------
        xpid: str
            Experiment ID for the requested date and targeted problem
        
        
        Notes
        -----
        Last doctring review: 17 Mar. 2022 (TR)
        """
        
        xpids = [x[0] for x in self.xpids] + [x[1] for x in self.xpids]
        
        foundIt = False
        for xpid in xpids:
            if rundate in self.xpid_rundates[xpid]:
                foundIt = True
                break
        
        if not foundIt:
            raise ValueError("Date is out of range "+rundate.strftime("%Y/%m/%d:%H"))
        
        return xpid
    
    
    def get_closest_rundate(self, reqdate, network="both", position="closest"):
        """Get the closest run date before the requested date
        
        
        Parameters
        ----------
        self: `rpe.settings.ReforecastSettings` instance
            Object containing data about the reforecast settings
            
        reqdate: datetime.datetime
            Requested date
        
        network: str
            Selected network. Options are "06" for the 6UTC run, "18" for the 18UTC
            run and "both" to return the closest of all runs.
        
        position: {"closest", "anterior", "posterior"}
            The function returns the closest anterior/posterior run date in stead
            of the closest.
        
        Returns
        -------
        rundate: datetime.datetime
            Date at which the forecast began
        
        
        Notes
        -----
        Last doctring review: 17 Mar. 2022 (TR)
        """
        
        xpids06 = [x[0] for x in self.xpids]
        xpids18 = [x[1] for x in self.xpids]
        
        foundIt = False
        
        for i_xp in range(len(xpids18)):
            if foundIt:
                break
            
            datemin, datemax = self.xpid_dateboundaries[xpids18[i_xp]]
            
            if reqdate < datemin or reqdate > datemax:
                continue
            
            if network=="06":
                rundates = self.xpid_rundates[xpids06[i_xp]]
            elif network=="18":
                rundates = self.xpid_rundates[xpids18[i_xp]]
            elif network=="both":
                rundates = self.xpid_rundates[xpids06[i_xp]] + self.xpid_rundates[xpids18[i_xp]]
            else:
                raise ValueError("Invalid network given:", network)
            
            rundates.sort()
            
            for i_rd in range(len(rundates)):
                if reqdate < rundates[i_rd]:
                    foundIt = True
                    break
                
            
        if not foundIt:
            raise ValueError("Date is out of range "+reqdate.strftime("%Y/%m/%d:%H"))
        
        if position == "anterior":
            idx = i_rd-1
        elif position == "posterior":
            idx = i_rd
        elif position == "closest":
            if abs(reqdate - rundates[i_rd]) > abs(reqdate - rundates[i_rd-1]):
                idx = i_rd-1
            else:
                idx = i_rd
        
        return rundates[idx]
    
    
    def get_rundates_between_dates(self, startdate, enddate, network="both", years="all", months="all", days="all"):
        """Return the list of rundates that exist between given dates.
        
        
        Parameters
        ----------
        self: `rpe.settings.ReforecastSettings` instance
            Object containing data about the reforecast settings
            
        startdate: datetime.datetime
            Starting date of the requested period
        
        enddate: datetime.datetime
            Ending date of the requested period
        
        network: str
            Selected network. Options are "06" for the 6UTC run, "18" for the 18UTC
            run and "both" to return the closest of all runs
        
        
        Returns
        -------
        rundates: datetime.datetime
            List of run dates (date at which the forecast began) within the
            requested period
        
        
        Notes
        -----
        Last doctring review: 17 Mar. 2022 (TR)
        """
        
        if network=="06":
            xpids = [x[0] for x in self.xpids]
        elif network=="18":
            xpids = [x[1] for x in self.xpids]
        elif network=="both":
            xpids = [x[0] for x in self.xpids] + [x[1] for x in self.xpids]
        else:
            raise ValueError("Invalid network given:", network)
        
        rundates = []
        for xpid in xpids:
            rundates += self.xpid_rundates[xpid]
        
        rundates.sort()
        rundates = np.array(rundates)
        inside = np.logical_and([d>startdate for d in rundates], [d<enddate for d in rundates])
        dex = inside
        
        if isinstance(years, list):
            inyears = [d.year in years for d in rundates]
            dex = np.logical_and(dex, inyears)
        
        if isinstance(months, list):
            inmonths = [d.month in months for d in rundates]
            dex = np.logical_and(dex, inmonths)
        
        if isinstance(days, list):
            indays = [d.day in days for d in rundates]
            dex = np.logical_and(dex, indays)
        
        return rundates[dex]
        
    
    def estimate_storage_volume(
            self,
            size_single_file,
            startdate=None,
            enddate=None,
            n_members=None,
            leadtimes=None,
            network="both",
        ):
        """Give an estimation of the storage volume of the reforecast
        
        
        Parameters
        ----------
        self: `rpe.settings.ReforecastSettings` instance
            Object containing data about the reforecast settings
            
        size_single_file: int
            Approximate volume of a single file (in Mo)
        
        startdate: datetime.datetime
            Starting date of the requested period
        
        enddate: datetime.datetime
            Ending date of the requested period
            
        n_members: int
            Number of members
        
        network: str
            Selected network. Options are "06" for the 6UTC run, "18" for the 18UTC
            run and "both" to return the closest of all runs
        
        
        Returns
        -------
        total_size: int
            Estimated volume of the reforecast (in Mo)
        
        
        Notes
        -----
        Values for size_single_file:
            netCDF glob025 float64: 24 Mo
            netCDF glob025 float32: 14 Mo
            netCDF glob025 float16: 6 Mo
        
        Last doctring review: 17 Mar. 2022 (TR)
        """
        if startdate is None:
            startdate=self.veryfirstdate
        if enddate is None:
            enddate=self.enddate
        if n_members is None:
            n_members=self.n_members
        if leadtimes is None:
            leadtimes=self.leadtimes
        
        rundates = self.get_rundates_between_dates(startdate, enddate, network=network)
        n_runs = rundates.size
        n_ldt = len(leadtimes)
        n_files = n_runs * n_members * n_ldt
        
        return size_single_file*n_files


# End of file
