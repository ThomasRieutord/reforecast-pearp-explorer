#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Tools to access the long-term PEARP reforecast made at CNRM/GMAP/RECYF.
Module to ease transferts among machines with batches of files.

@author: Rieutord Thomas
@creationdate: 2022/05/02

Global docstrings reviews:
  * 07 Jun. 2022 by Thomas Rieutord (TR)
"""
import os
import time
import pickle
import numpy as np
from reforecast_pearp_explorer.settings import ReforecastSettings

rfs = ReforecastSettings()

def create_batches(array, batch_size=None, n_batches=None):
    """Split a list in several batches
    
    
    Parameters
    ----------
    array: array-like
        Full array to be split into batches
    
    batch_size: int
        Desired number of elements in a batch
        
    n_batches: int
        Desired number of batches
    
    
    Returns
    -------
    batches: list of array-like
        List of size `n_batches` containing pieces of `array`
    
    
    Notes
    -----
    Exactly one of the arguments batch_size and n_batches must be specified
    """
    
    if n_batches is not None:
        if batch_size is not None:
            raise ValueError("Only one of the arguments batch_size and n_batches must be specified")
        else:
            batch_size = len(array) // n_batches + 1
    else:
        if batch_size is not None:
            n_batches = len(array) // batch_size + 1
        else:
            raise ValueError("Please specify at least one of the arguments batch_size or n_batches")
        
    
    batches = []
    for ib in range(n_batches):
        idxmin = max(0, ib*batch_size)
        idxmax = min(len(array), (ib+1)*batch_size)
        if len(array[idxmin:idxmax])>0:
            batches.append(array[idxmin:idxmax])
        
    return batches

class BatchLoader():
    """The class `BatchLoader` is made to manage the transfert of reforecast files
    with batches. The use of batches is mandatory to transfert a large amount of
    files.
    
    
    Parameters
    ----------
    self: `BatchLoader` object
        Contain all necessary material (attributes and methods) to perform
        the transfert of reforecast files with batches.
    
    initial_batches: list of `ReforecastBatch`
        The list of reforecast batches organized by this instance of `BatchLoader`
    
    picklestorepath: str
        Path to the Pickle objects that contains batches and batch loader
    
    middlestorepath: str
        Path to the main storage directory on the execution host
    
    middlehost: str
        Designation (resolved name or IP adress) of the execution host. The
        execution host is the machine where the scripts and crons are executed
        and where the batched of data are stored transiently
    
    targethost: str
        Designation (resolved name or IP adress) of the final host. The final
        host is the final destination of the data
    
    prefixdir: str
        Directory name to distinguish several purpose of use
    
    removegribs: bool
        If True, the GRIB files are removed after extraction into netCDF
    
    
    Attributes
    ----------
    Copied from Parameters:
        initial_batches, picklestorepath, middlestorepath, middlehost, targethost, prefixdir
    
    n_batches: int
        Number of batches
    
    picklefilename: str
        Name of the Pickle file containing the batch organizer
        
    current_step: {"source2middle", "middle2target"}
        Designation of the current step
    
    current_batch_source2middle: int
        Current batch number for the step "source2middle"
        
    current_batch_middle2target: int
        Current batch number for the step "middle2target"
    
    status_source2middle: list of bool of size `self.n_batches`
        Status of all batches for the step "source2middle". Batches with a status
        at True have been processed.
        
    status_middle2target: list of bool of size `self.n_batches`
        Status of all batches for the step "middle2target". Batches with a status
        at True have been processed.
        
    """
    
    def __init__(self, initial_batches, picklestorepath, middlestorepath, prefixdir=None, removegribs=False, middlehost=None, targethost=None):
        """Constructor of `BatchLoader`"""
        self.initial_batches = initial_batches
        self.picklestorepath = picklestorepath
        self.middlestorepath = middlestorepath
        self.prefixdir = prefixdir
        self.removegribs = removegribs
        # Fixed or deduced attributes
        self.picklefilename = "batch_organizer.pkl"
        self.n_batches = len(initial_batches)
        self.current_step = "source2middle"
        self.current_batch_source2middle = 0
        self.current_batch_middle2target = 0
        self.status_source2middle = [False for b in range(self.n_batches)]
        self.status_middle2target = [False for b in range(self.n_batches)]
        # Informative only (not used in any class nor program)
        self.middlehost = middlehost
        self.targethost = targethost
        
    def __len__(self):
        """Number of batches"""
        return self.n_batches
        
    def __getitem__(self, idx):
        """Access the batch number `idx`
        """
        
        if idx is not None:
            with open(os.path.join(self.picklestorepath, f"batch_{idx}.pkl"), "rb") as f:
                rfb = pickle.load(f)
        else:
            rfb = None
        
        return rfb
        
    def __str__(self):
        """Information to display with the `print` function"""
        return "\n".join(
            [
                attr + "=" + str(getattr(self,attr)) for attr in [
                    "n_batches",
                    "current_step",
                    "current_batch_source2middle",
                    "current_batch_middle2target",
                    "picklestorepath",
                    "status_source2middle",
                    "status_middle2target",
                ]
            ]
        )
        
    def create_picklestore(self):
        """Create the Pickle files containing the batches and the batch loader."""
        mainfile = os.path.join(self.picklestorepath, self.picklefilename)
        with open(mainfile, "wb") as f:
            pickle.dump(self,f)
        
        os.chmod(mainfile, 0o666)
        
        for batch in self.initial_batches:
            batch.update_pickle_file(self.picklestorepath)
        
        self.create_trigger_file("source2middle")
    
    def iter_batch_number(self, stepname):
        """Iterate the batch number.
        
        
        Parameters
        ----------
        self: `BatchLoader` object
            Contain all necessary material (attributes and methods) to perform
            the transfert of reforecast files with batches.
        
        stepname: {"source2middle", "middle2target"}
            Designation of the step to iterate.
            
        
        Returns
        -------
        self: `BatchLoader` object
            Updated batch loader
        """
        batch_number = getattr(self,"current_batch_"+stepname)
        status = getattr(self,"status_"+stepname)
        
        status[batch_number] = True
        
        batch_number += 1
        if batch_number == self.n_batches:
            batch_number = None
        
        setattr(self,"current_batch_"+stepname, batch_number)
        setattr(self,"status_"+stepname, status)
        
        return self
    
    def iter_stepname(self):
        """Iterate the step.
        
        There are two consecutive steps in this transfert:
            "source2middle" makes the transfert from Hendrix to the execution host
            "middle2target" makes the transfert from the execution host to the final host
        """
        if self.current_step == "source2middle":
            self.current_step = "middle2target"
        elif self.current_step == "middle2target":
            self.current_step = "source2middle"
        else:
            raise ValueError("Invalid stepname. Possible values are {'source2middle','middle2target'}")
        
        return self
        
    def get_current_batch(self):
        """Return the batch that is currently transfered"""
        batch_number = getattr(self,"current_batch_"+self.current_step)
        return self[batch_number]
        
    def update_pickle_file(self):
        """Write the batch loader in its current state into the Pickle file"""
        with open(os.path.join(self.picklestorepath, self.picklefilename), "wb") as f:
            pickle.dump(self,f)
    
    def create_trigger_file(self, stepname, triggerdir = None):
        """Create the file that will trigger the next step (when the file is present,
        a cron launches the execution of the next step).
        
        
        Parameters
        ----------
        self: `BatchLoader` object
            Contain all necessary material (attributes and methods) to perform
            the transfert of reforecast files with batches.
        
        stepname: {"source2middle", "middle2target"}
            Designation of the step to iterate.
        
        triggerdir: str
            Path of the directory in which the trigger file will be created.
            Default is BatchLoader.picklestorepath
        
        
        Returns
        -------
        None
        """
        if stepname not in ["source2middle", "middle2target"]:
            raise ValueError("Invalid stepname. Possible values are {'source2middle','middle2target'}")
        
        if triggerdir is None:
            triggerdir = self.picklestorepath
        
        batch_number = getattr(self,"current_batch_"+stepname)
        if batch_number is not None:
            with open(os.path.join(triggerdir, stepname+"_go_to_next_batch.txt"), "w") as f:
                f.write(time.ctime())
                f.write("\nfinished batch="+str(batch_number))
            
            os.chmod(os.path.join(triggerdir, stepname+"_go_to_next_batch.txt"), 0o666)
        else:
            print("All batches are completed")
    
    def terminate_step(self):
        """Execute all methods mandatory to terminate a step of the transfert"""
        
        self.iter_batch_number(self.current_step)
        self.iter_stepname()
        self.update_pickle_file()
        self.create_trigger_file(self.current_step)
        
        return self



class ReforecastBatch():
    """The class `ReforecastBatch` represents batches of reforecast files that
    are identified by their rundate, leadtime, member and domain.
    
    
    Parameters
    ----------
    self: `ReforecastBatch` object
        Identification of several reforecast files
    
    batch_number: int
        Number of the batch
    
    rundates: list of `datetime.datetime`
        List of rundates that are part of the batch
    
    members: list of int
        List of members that are part of the batch. Default is all members
    
    leadtimes: list of `datetime.timedelta`
        List of lead times that are part of the batch. Default is all lead times
    
    domains: list of str
        List of domains that are part of the batch.
    
    dtype: `numpy.dtype` object, default=numpy.float64
        Specify the data type (e.g. put np.float16 to save memory)
    
    rfs: `ReforecastSettings` object
        Object containing all data about the reforecast settings
    
    
    Attributes
    ----------
    Copied from Parameters:
        batch_number, rundates, members, leadtimes, domains
    
    picklefilename: str
        Name of the Pickle file containing the batch
    """
    
    def __init__(self, batch_number, rundates, members=None, leadtimes=None, domains=["eurat01", "glob025"], dtype=np.float64, rfs=rfs):
        """Constructor of `ReforecastBatch`"""
        
        if members is None:
            self.members = [i for i in range(rfs.n_members)]
        else:
            self.members = members
        
        if leadtimes is None:
            self.leadtimes = rfs.leadtimes
        else:
            self.leadtimes = leadtimes
        
        self.batch_number = batch_number
        self.rundates = rundates
        self.domains = domains
        self.dtype = dtype
        self.picklefilename = f"batch_{self.batch_number}.pkl"
    
    def __len__(self):
        """Number of individual files in the batch"""
        return len(self.rundates)*len(self.leadtimes)*len(self.members)
    
    def __str__(self):
        """Information to display with the `print` function"""
        return "\n".join(
            [
                "Reforecast batch #"+str(self.batch_number),
                str(len(self.members)) + " members",
                str(len(self.leadtimes)) + " leadtimes",
                str(len(self.rundates)) + " rundates (" + self.rundates[0].strftime("%Y-%m-%d %Hh") + " -> " + self.rundates[-1].strftime("%Y-%m-%d %Hh") + ")",
            ]
        )
    
    def update_pickle_file(self, picklestorepath):
        """Write the batch in its current state into the Pickle file
        
        
        Parameters
        ----------
        picklestorepath: str
            Path to the Pickle objects that contains batches and batch loader
        """
        with open(os.path.join(picklestorepath, self.picklefilename), "wb") as f:
            pickle.dump(self,f)
        
