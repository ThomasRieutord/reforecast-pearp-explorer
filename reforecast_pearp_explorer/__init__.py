#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Package to access and exploit PEARP reforecast

url=https://gitlab.com/ThomasRieutord/reforecast-pearp-explorer
"""
__version__="0.1.1"
