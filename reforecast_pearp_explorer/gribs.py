#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Tools to access the long-term PEARP reforecast made at CNRM/GMAP/RECYF.
Module to manipulate GRIB files.

@author: Rieutord Thomas
@date: 2022/01/13

Global docstrings reviews:
  * 01 Mar. 2022 by Thomas Rieutord (TR)
  * 17 Mar. 2022 by Thomas Rieutord (TR)
"""
import os.path
import datetime as dt
import numpy as np
import time
import netCDF4 as nc

import epygram
import metpy.calc as mpcalc
from metpy.units import units

from reforecast_pearp_explorer.settings import ReforecastSettings

rfs = ReforecastSettings().build()


fields_to_check = rfs.all_fieldnames
fields_to_handgrip = rfs.all_field2gribid
import logging
logger = logging.getLogger()
logger.setLevel(logging.CRITICAL)

def fields_summary(gribobj, select=None, printit=False):
    """Give a summary of the fields present in a GRIB file.
    
    
    Parameters
    ----------
    gribobj: `epygram.formats.GRIB.GRIB` object
        EPyGrAM object returned by the `epygram.formats.resource` function
    
    select: dict or None
        When provided, the `select` parameter must be a partial handgrip. The
        fields in the summary will be the ones matching this partial handgrip.
        Ex: select={"shortName":"z"} to select only geopotential fields.
    
    printit: bool
        If True, the result is displayed with a print
    
    
    Returns
    -------
    result: str
        Message with all the fields and their levels
    
    
    Notes
    -----
    Last doctring review: 17 Mar. 2022 (TR)
    """
    result = ""
    oldfname = None
    for f in gribobj.listfields(select=select):
        fname=f['name']
        if fname != oldfname:
            result += f['name']+" ("+f['shortName']+")\n"
        
        oldfname = fname
        try:
            result += "   "+str(f['level'])+"\n"
        except KeyError:
            continue
    
    if printit:
        print(result)
    
    return result

def display_fields_params(go, sel=None, keys = ["parameterNumber", "shortName", "name", "typeOfFirstFixedSurface", "level"]):
    """Show the description of fields in GRIB files opened with Epygram.
    
    
    Parameters
    ----------
    go: `epygram.format.GRIB` object
        Object containing all information accessible from the GRIB file. Returned
        by the method `epygram.formats.resource`
    
    sel: dict
        Handgrip (dict containing GRIB keys and their values). The function displays
        only the fields of the GRIB file matching with this handgrip
    
    keys: list of str
        GRIB keys to display
    
    
    Returns
    -------
    None. Output printed in the console
    
    
    Notes
    -----
    Last doctring review: 17 Mar. 2022 (TR)
    """
    lf = go.listfields(select=sel)
    for f in lf:
        msg = []
        for k in keys:
            try:
                msg.append(k + "=" + str(f[k]))
            except KeyError:
                continue
            
        print("\t".join(msg))

def handgrip_matches(handgrip, gribobj):
    """Count the number of fields matching the given handgrip in the GRIB file.
    
    
    Parameters
    ----------
    handgrip: dict
        Identification of the field. Dict with enough keys to uniquely define
        the requested field. Refer to the EPyGrAM documention for more information.
    
    gribobj: `epygram.formats.GRIB.GRIB` object
        EPyGrAM object returned by the `epygram.formats.resource` function
    
    
    Returns
    -------
    n_matches: int
        Number of fields matching the given handgrip
    
    
    Notes
    -----
    Last doctring review: 17 Mar. 2022 (TR)
    """
    
    if "calculated" in handgrip.keys():
        n_matches = 1
    else:
        n_matches = len(gribobj.listfields(select=handgrip))
    return n_matches

def get_domain_from_filename(filename):
    """Extract the name of the domain from a file name as they are in output of
    ARPEGE.
    
    
    Parameters
    ----------
    filename: str
        Path of file name of the form `grid.arpege-forecast.eurat01+0030:00.grib`
        The domain to be extracted must be between "+" and "."
    
    
    Returns
    -------
    domain: str
        Geographical domain
    
    
    Notes
    -----
    Last doctring review: 17 Mar. 2022 (TR)
    """
    
    _, fn = os.path.split(filename)
    domain = fn.split("+")[0].split(".")[-1]
    
    return domain.lower()

def get_member_from_path(filepath, fromfilename=False, mbdirdex=None):
    """Extract the number of member from the path to the file
    """
    head, tail = os.path.split(filepath)
    if fromfilename:
        # If the member is precised in the filename
        # Must be of the form "arpege_2010032906_mb000.EURAT01+0003:00.grib"
        mbidx = tail.index("mb")
        member = tail[mbidx+2:mbidx+5]
    else:
        pathpieces = []
        while len(tail)>0:
            pathpieces.append(tail)
            head, tail = os.path.split(head)
        pathpieces = pathpieces[::-1]
        
        if isinstance(mbdirdex,int): # Custom path
            mbdir = pathpieces[mbdirdex]
        elif pathpieces[0] == "cnrm": # LUSTRE path
            mbdir = pathpieces[-2]
        elif pathpieces[2] == "vortex": # VORTEX path
            mbdir = pathpieces[-3]
        else:
            raise ValueError("Unable to find the member from the path "+filepath)
        
        member = int(mbdir[2:])
    
    return member

def get_field(go, fd, subdom_borders=None, res = 0.25):
    """Get requested field from the provided GRIB object.
    
    Possibily to extract only a subdomain (if the variable `subdom_borders` is provided).
    For some fields, few calculations are made. The field ID is then created
    afterward and may not be standard.
    
    Parameters
    ----------
    go: `epygram.format.GRIB` object
        Object containing all information accessible from the GRIB file. Returned
        by the method `epygram.formats.resource`
    
    fd: str
        Short name of the requested field. Must be known in settings.py
    
    subdom_borders: None or dict
        Dict of the form dict(lonmin=, latmin=, lonmax=, latmax=). If provided,
        only the subdomain defined by the rectangle will be extracted.
        
    res: float
        Resolution of the lon-lat grib in degrees. Supposed to be a divisor of
        lonmax-lonmin and latmax-latmin.
    
    
    Returns
    -------
    field: `epygram.field` object
        Requested field (data and metadata). See EPyGrAM doc
    
    Notes
    -----
    Last doctring review: 17 Mar. 2022 (TR)
    """
    
    if isinstance(subdom_borders, dict):
        def readfieldfunc(go, hg):
            f = epygram.formats.GRIB.GRIB.readfield(go,hg)
            return f.resample_on_regularll(borders=subdom_borders, resolution_in_degrees=res)
    else:
        def readfieldfunc(go, hg):
            return epygram.formats.GRIB.GRIB.readfield(go,hg)
    
    
    if fd=='snowfall_amount':
        hg1 = {"discipline":0,"parameterCategory":1,"parameterNumber":55, "shortName":"csrwe"}
        field1=readfieldfunc(go,hg1)
        
        hg2 = {"discipline":0,"parameterCategory":1,"parameterNumber":56, "shortName":"lssrwe"}
        field2=readfieldfunc(go,hg2)
        if isinstance(subdom_borders, dict):
            field2 = field2.resample_on_regularll(subdom_borders, resolution_in_degrees=res)
        
        field = field1 + field2
        field.fid['GRIB2'] = {
            'name': 'Total snowfall amount since the start of the run',
            'shortName': 'tsrwe',
            'discipline': 0,
            'parameterCategory': 1,
            'parameterNumber': 53,
        }
        
    elif fd=='precipitation_amount':
        hg1 = {"discipline":0,"parameterCategory":1,"parameterNumber":55, "shortName":"csrwe"}
        field1=readfieldfunc(go,hg1)
        if isinstance(subdom_borders, dict):
            field1 = field1.resample_on_regularll(subdom_borders, resolution_in_degrees=res)
        
        hg2 = {"discipline":0,"parameterCategory":1,"parameterNumber":56, "shortName":"lssrwe"}
        field2=readfieldfunc(go,hg2)
        if isinstance(subdom_borders, dict):
            field2 = field2.resample_on_regularll(subdom_borders, resolution_in_degrees=res)
        
        hg3 = {"discipline":0,"parameterCategory":1,"parameterNumber":76}    # ! shortName=unknown
        field3=readfieldfunc(go,hg3)
        
        hg4 = {"discipline":0,"parameterCategory":1,"parameterNumber":77}    # ! shortName=unknown
        field4=readfieldfunc(go,hg4)
        
        field = field1 + field2 + field3 + field4
        field.fid['GRIB2'] = {
            'name': 'Total precipitation amount since the start of the run',
            'shortName': 'tp',
            'discipline': 0,
            'parameterCategory': 1,
            'parameterNumber': 52,
        }
    elif fd=='total_convective_precipitation_amount':
        
        # Convective snowfall rate water equivalent
        hg1 = {"discipline":0,"parameterCategory":1,"parameterNumber":55, "shortName":"csrwe"}
        field1=readfieldfunc(go,hg1)
        if isinstance(subdom_borders, dict):
            field1 = field1.resample_on_regularll(subdom_borders, resolution_in_degrees=res)
        
        # Convective liquid precipitation
        hg3 = {"discipline":0,"parameterCategory":1,"parameterNumber":76}    # ! shortName=unknown
        field3=readfieldfunc(go,hg3)
        
        field = field1 + field3
        field.fid['GRIB2'] = {
            'name': 'Total convective precipitation amount since the start of the run',
            'shortName': 'tcp',
            'discipline': 0,
            'parameterCategory': 1,
            'parameterNumber': 10,
        }
    elif fd=='wind_speed_at_10_meters':
        hg1 = {"discipline":0,"parameterCategory":2,"parameterNumber":2, "shortName":"10u", "typeOfFirstFixedSurface":103}
        field1=readfieldfunc(go,hg1)
        
        hg2 = {"discipline":0,"parameterCategory":2,"parameterNumber":3, "shortName":"10v", "typeOfFirstFixedSurface":103}
        field2=readfieldfunc(go,hg2)
        
        field = epygram.fields.make_vector_field(field1,field2).to_module()
        
        field.fid['GRIB2'] = {
            'name': '10 metre wind speed',
            'shortName': '10ff',
            'discipline': 0,
            'parameterCategory': 2,
            'parameterNumber': 1,
            'level': 10
        }
        
    elif fd=='wind_speed_of_gust_at_10_meters':
        hg1 = {"discipline":0,"parameterCategory":2,"parameterNumber":23, "shortName":"ugust", "lengthOfTimeRange":1}
        field1=readfieldfunc(go,hg1)
        
        hg2 = {"discipline":0,"parameterCategory":2,"parameterNumber":24, "shortName":"vgust", "lengthOfTimeRange":1}
        field2=readfieldfunc(go,hg2)
        
        field = epygram.fields.make_vector_field(field1,field2).to_module()
        
        field.fid['GRIB2'] = {
            'name': '10 metre wind gust for last hour',
            'shortName': '10gust',
            'discipline': 0,
            'parameterCategory': 2,
            'parameterNumber': 23,
            'level': 10
        }
        
    elif 'air_potential_temperature' in fd:
        plvl = fd.split('_')[-2]
        hg_t = fields_to_handgrip["air_temperature_at_" + plvl + "_hPa"]
        ftemperature = readfieldfunc(go,hg_t)
        temperature = ftemperature.getdata() * units.kelvin
        
        pressure = int(plvl) * np.ones_like(temperature) * units.hPa
        
        pot_temperature = mpcalc.potential_temperature(pressure, temperature)
        
        field = ftemperature.deepcopy()
        field.data = pot_temperature
        field.fid['GRIB2'] = {
            'editionNumber': 2,
            'name': 'Potential temperature',
            'shortName': 'pt',
            'discipline': 0,
            'parameterCategory': 0,
            'parameterNumber': 2,
            'typeOfFirstFixedSurface': 100,
            'level':int(plvl),
            'typeOfSecondFixedSurface': 255,
            'tablesVersion': 15,
            'productDefinitionTemplateNumber': 1
        }
        field.fid['generic'] = {}
        field.fid['short'] = {}
    
    elif 'relative_vorticity' in fd:
        hg = rfs.all_field2gribid[fd]
        field=readfieldfunc(go,hg)
        plvl = fd.split('_')[-2]
        field.fid['GRIB2'] = {
            'editionNumber': 2,
            'name': 'Relative vorticity',
            'shortName': 'vo',
            'discipline': 0,
            'parameterCategory': 2,
            'parameterNumber': 12,
            'typeOfFirstFixedSurface': 100,
            'level':int(plvl),
            'typeOfSecondFixedSurface': 255,
            'tablesVersion': 15,
            'productDefinitionTemplateNumber': 1
        }
    else:
        hg = rfs.all_field2gribid[fd]
        
        if hg is None:
            print("Field ", fd, " is missing")
            field=None
        
        field=readfieldfunc(go,hg)
    
    field.fid['CF'] = fd
    
    return field


def check_reqfields(go, reqfields="default", nzthreshold=10):
    """Check basic statistics on the required fields: min, max, mean and std are
    not NaN and number of non-zeros is above a given threshold.
    
    
    Parameters
    ----------
    go: `epygram.format.GRIB` object
        Object containing all information accessible from the GRIB file. Returned
        by the method `epygram.formats.resource`
    
    reqfields: list or "default"
        List of the fields to check. Elements of the list must be taken among
        the list rfs.all_fieldnames. The default setting is to take the entire
        list rfs.all_fieldnames.
    
    nzthreshold: int
        Minimum number of non-zeros values to consider fields as valid
    
    
    Returns
    -------
    allOK: bool
        True is all the required fields are considered as valid
    
    
    Notes
    -----
    Last doctring review: 17 Mar. 2022 (TR)
    """
    
    if reqfields=="default":
        reqfields = fields_to_check
    
    areOK = []
    for fd in reqfields:
        
        field = get_field(go, fd)
        
        stats = field.stats()
        isOK = stats['nonzero']>nzthreshold and np.isnan([stats['min'],stats['max'],stats['mean'],stats['std']]).sum()==0
        areOK.append(isOK)
        
        if not isOK:
            print("Invalid field:",field.fid['GRIB2'])
    
    return all(areOK)


def drop_to_netcdf(gribpath, netcdfpath="default", reqfields="default", subdom_borders=None, res=0.25, dtype = np.float64):
    """Drop the requested fields into a netCDF4 file
    
    Parameters
    ----------
    gribpath: str
        Path to the source GRIB file
        
    netcdfpath: str
        Path to the target netCDF file. If not provided, the same name as the GRIB file will be taken.
    
    reqfields: list or "default"
        List of the fields to have in the netCDF file. Elements of the list must be taken among
        the list rfs.all_fieldnames. The default setting is to take the entire
        list rfs.all_fieldnames.
        
    subdom_borders: None or dict
        Dict of the form dict(lonmin=, latmin=, lonmax=, latmax=). If provided,
        only the subdomain defined by the rectangle will be extracted.
        
    res: float
        Resolution of the lon-lat grib in degrees. Supposed to be a divisor of
        lonmax-lonmin and latmax-latmin.
    
    dtype: `numpy.dtype`
        Data type of the elements in the Numpy arrays. Choosing a coarser data type
        (e.g. float16 instead of float64) improves compression
    
    Returns
    -------
    None. The netCDF file is created.
    
    
    Notes
    -----
    Last doctring review: 17 Mar. 2022 (TR)
    """
    
    if reqfields=="default":
        domain = get_domain_from_filename(gribpath)
        reqfields = rfs.fields_to_extract[domain]
    
    if netcdfpath=="default":
        netcdfpath = gribpath[:-5] + ".nc"
    
    go = epygram.formats.resource(gribpath, openmode="r")
    nco = epygram.formats.resource(netcdfpath, fmt='netCDF', openmode="w")
    nco.set_global_attributes(
        title = "Reforecast PEARP cy46t1 output",
        source = "ARPEGE PEARP cy46t1",
        institution = "Meteo-France CNRM/GMAP/RECYF",
        history = "Created the " + time.ctime(time.time()) + ". Extracted from the GRIB file "+gribpath,
        contactperson = "Thomas Rieutord (thomas.rieutord@meteo.fr)",
    )
    
    miss = []
    for fd in reqfields:
        try:
            field = get_field(go, fd, subdom_borders=subdom_borders, res=res)
            field.fid['netCDF']=fd
            field.data = np.array(field.data, dtype=dtype)
            nco.writefield(field)
        except epygram.epygramError:
            print("Error with the field "+fd+". Not exported in netCDF.")
            miss.append(fd)
    
    nco.validity = field.validity.deepcopy()
    try:
        nco.member = get_member_from_path(go.filename)
    except ValueError:
        nco.member = get_member_from_path(go.filename, fromfilename=True)
    
    nco.close()
    print("Netcdf file with ",len(reqfields)-len(miss)," fields written in ",netcdfpath)

# End of file
