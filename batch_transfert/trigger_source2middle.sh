#!/usr/bin/bash
# source2middle


source ~/.bash_profile
source ~/.rpe_hosts_and_paths
source $CONDAPATH
conda activate rpe

if [ -e $PICKLESTOREPATH/source2middle_go_to_next_batch.txt ]
then
    echo " =*=*=*=*=*=*=*=*=*= > Starting transfert from Hendrix to $MIDDLEHOST" >> $PICKLESTOREPATH/transferts.log
    date >> $PICKLESTOREPATH/transferts.log
    rm -f $PICKLESTOREPATH/source2middle_go_to_next_batch.txt
    # To uncomment when the middle host must be flushed at each batch
    #rm -rf $MIDDLESTOREPATH/*
    python --version >> $PICKLESTOREPATH/transferts.log
    python $SCRIPTSPATH/transfert_source2middle.py >> $PICKLESTOREPATH/transferts.log
    python $SCRIPTSPATH/update_batches.py >> $PICKLESTOREPATH/transferts.log
    echo " < *=*=*=*=*=*=*=*=*= Ending transfert from Hendrix to $MIDDLEHOST" >> $PICKLESTOREPATH/transferts.log
    date >> $PICKLESTOREPATH/transferts.log
fi
