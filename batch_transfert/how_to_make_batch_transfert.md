HOW TO MAKE BATCH TRANSFERT
===========================

Summary
-------
  1. Install the package
  2. Check paths and connections
  3. Define the batches
  4. Set up the crons
  5. Check advancement

Purpose
-------
The transfert with batch enable to transfert large volume of data by splitting the full dataset into small batches.
This program performs batch transfert of PEARP reforecast data.
The data to be transfered is identified by a **set of dates, members, leadtimes and domains**.
The data is originated from Hendrix, the High Performance Storage System of Meteo-France.
It will be transfered to a **target host** (which must have enough space to store the full dataset).
Only the selected fields (see `reforecast_pearp_explorer.settings.ReforecastSettings.fields_to_extract` for the default list) are extracted and the final format is netCDF4.
The extraction from Hendrix and the conversion to netCDF is made on a **middle host**.
The **batch size** must be chosen according to the storage capacity of the middle host.

Are highlighted in the text the main elements to set up the batch transfert, namely:
  * a set of dates, members, leadtimes and domains
  * a target host
  * an middle host
  * a batch size

The transfert is performed from one network to another, as represented on the diagram below.

![Alt](illustration_transfert.png)

One user (let say "Thomas") will perform the tranfert from the source (Hendrix) to the middle host.
Thomas must have access to a machine allowed to read and download files from Hendrix and to write on the middle host.
However, he usually do not have access to the target host.
The transfert from Hendrix to the middle host is perform with Météo-France's package Vortex, which uses FTP protocol.
Thomas' role in the batch transfert is identified as *source2middle*.

Another user (let say "Florian") will perform the transfert from the middle host to the target host.
Florian must have access to a machine allowed to read and download files from the middle host and to write on the target host.
However, he usually do not have access to Hendrix.
The transfert from the middle host to the target is perform with `rsync`, which uses SSH protocol.
Florian's role in the batch transfert is identified as *middle2target*.


Install the package
-------------------
Batch transferts are based on the package `reforecast_pearp_explorer`.
The installation procedure is explained in the README of the root directory.
An online version is available on the Gitlab repository: https://gitlab.com/ThomasRieutord/reforecast-pearp-explorer
The package must be installed by all users on the machines accessing the middle host.

### Installation on Belenos

If Belenos is used as middle host, the restrictions on the network will not allow the cloning of the package.
The package must be downloaded on a less restricted server (e.g. sxrecyf) an then pushed onto Belenos (e.g. with rsync).

Check paths
-----------
The generic command line to execute is the following.
```
python init_paths2.py <prefixdir> <role> <cleanup|targethost>
```
where `<prefixdir>` is the name of the prefix directory, `<role>` is the role of the user running the command (either "source2middle" or "middle2target") and the third argument is the target host if role="middle2target" (mandatory) or a boolean for cleaning or not (optional).

To start a batch transfert, it must be executed by both users (Thomas and Florian).

Let say we want to start a batch transfert with `prefixdir=myfirsttransfert`
First, the user *source2middle* (Thomas) run
```
python init_paths2.py myfirsttransfert source2middle true
```
The expected output is
```
Create directory /cnrm/recyf/NO_SAVE/Data/users/rieutordt/reforecast-pearp-cy46t1/myfirsttransfert
Cleaning directory /cnrm/recyf/NO_SAVE/Data/users/rieutordt/reforecast-pearp-cy46t1/toolsync
All paths are correctly set!
  --- Next steps ---
 (3/5) Define the batches: `python batches_definition.py`
 (4/5) Set up the crons: copy the following line and paste it into the editor of `crontab -e`:
 * * * * * /d0/Users/rieutordt/gitsync/reforecast-pearp-explorer/batch_transfert/trigger_source2middle.sh
 (5/5) Check advancement: `python check_batch_loader.py`
```


Then, the user *middle2target* (Florian) run
```
python init_paths2.py myfirsttransfert middle2target user@targethost.full.url
```
The expected output is
```
Check consistency between role middle2target and source2middle: OK.
All paths are correctly set!
  --- Next steps ---
 (3/5) Define the batches: done by the user with role=source2middle (you have nothing to do)
 (4/5) Set up the crons: copy the following line and paste it into the editor of `crontab -e`:
 * * * * * /d0/Users/pantillonf/reforecast-pearp-explorer/batch_transfert/trigger_middle2target.sh
 (5/5) Check advancement: `python check_batch_loader.py`
```

For both users, the small file `~/.rpe_hosts_and_paths` containing all necessary paths is created.


Define the batches
------------------
The program `batches_definition.py` create the batches.
The previously listed parameters are set at the beginning of this program (lines framed as "VARIABLE PART").
It is executed by the user *source2middle* and it must be updated at each new batch tranfert.

```
python batches_definition.py
```
The output must look like this:
```
Creating batches for rundates from  2018-09-24 00:00:00 to 2018-10-26 00:00:00
Batch 1/7 of size 2
Batch 2/7 of size 2
Batch 3/7 of size 2
Batch 4/7 of size 2
Batch 5/7 of size 2
Batch 6/7 of size 2
Batch 7/7 of size 1
All batches created at /cnrm/recyf/NO_SAVE/Data/users/rieutordt/reforecast-pearp-cy46t1/toolsync
  --- Next steps ---
 (4/5) Set up the crons: copy the output of `cat crontab_command.txt` and paste it into the editor of `crontab -e`
 (5/5) Check advancement: `python check_batch_loader.py`
```


Set up the crons
----------------
Crons are little programs running in background.
They are used in order to automate the tranfert.
The command must be copied from the file named `crontab_command.txt`.
For both users, the commands to execute are the following:
```
cat crontab_command.txt
<copy output>
crontab -e
<paste>
```

The transfert will start the next minute after the editor of crontab is closed.

### Additional infos

When executing `crontab -e` for the first time, it may be necessary to choose an editor (e.g. VIM) and to delete comment lines before pasting.
Be careful after pasting that the crontab definition file has exactly two lines. Sometimes, when pasting, the output of `cat crontab_command.txt` spread on more lines than necessary.


Check advancement
-----------------
As soon as the cron are set up, the transfert will start in the next minute.
To check the transfert, run
```
python check_batch_loader.py
```
Indications about how to read the output of this program are given in the docstring of the program.

Another possibility to check if the files are arriving as they are supposed to is to look at the directory $targetstorepath on the target host.
