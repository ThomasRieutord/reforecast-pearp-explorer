#!/home/rieutordt/anaconda3/envs/rpe/bin/python
# -*- coding: utf-8 -*-
"""Check the batch loader"""

import os
import sys
import time
import datetime as dt
import pickle

print("\n----------------------------------")
print(time.ctime())
print("----------------------------------")

with open(os.path.expanduser("~/.rpe_hosts_and_paths"), "r") as f:
    lines = f.readlines()
for l in lines:
    if "PICKLESTOREPATH" in l:
        picklestorepath = l.strip().split("=")[1]

print("Listing of " + picklestorepath)
for fl in os.listdir(picklestorepath):
    print(fl)

with open(os.path.join(picklestorepath, "batch_organizer.pkl"), "rb") as f:
    bo = pickle.load(f)


print("\n----------------------------------")
print("Batches info")
print("----------------------------------")
print(bo)
rfb = bo.get_current_batch()
print(" --- Current batch --- ")
print(rfb)

logfile = os.path.join(picklestorepath, "transferts.log")
print("\n----------------------------------")
print("Last modifications of " + logfile)
print("----------------------------------")
lastmodif = dt.datetime.fromtimestamp(os.stat(logfile).st_mtime)
print("Last modified:", lastmodif)
print("20 last lines:")
os.system("tail -20 " + logfile)

print("\n----------------------------------")
print("Transfered files at " + bo.middlestorepath)
print("----------------------------------")
print("Disk usage of " + bo.middlestorepath)
os.system("du -h --max-depth=1 " + bo.middlestorepath)

# print("Listing of " + bo.middlestorepath)
# for fl in os.listdir(bo.middlestorepath):
    # print(fl)
