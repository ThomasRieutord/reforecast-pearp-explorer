#!/usr/bin/bash
# middle2target 


source ~/.rpe_hosts_and_paths
source $CONDAPATH
conda activate rpe

if [ -e $PICKLESTOREPATH/middle2target_go_to_next_batch.txt ]
then
    echo " ==============> Starting transfert from $MIDDLEHOST to $TARGETHOST" >> $PICKLESTOREPATH/transferts.log
    rm -f $PICKLESTOREPATH/middle2target_go_to_next_batch.txt
    echo "\n-Sync command: rsync -avz $MIDDLESTOREPATH/ $TARGETHOST:$TARGETSTOREPATH/" >> $PICKLESTOREPATH/transferts.log
    rsync -avz $MIDDLESTOREPATH/ $TARGETHOST:$TARGETSTOREPATH/ >> $PICKLESTOREPATH/transferts.log
    python $SCRIPTSPATH/update_batches.py >> $PICKLESTOREPATH/transferts.log
    echo " <============== Ending transfert from $MIDDLEHOST to $TARGETHOST" >> $PICKLESTOREPATH/transferts.log
fi
