#!/home/rieutordt/anaconda3/envs/rpe/bin/python
# -*- coding: utf-8 -*-
"""Simple progression bar with chrono"""

import os
import sys
import time
import pickle

with open(os.path.expanduser("~/.rpe_hosts_and_paths"), "r") as f:
    lines = f.readlines()
for l in lines:
    if "PICKLESTOREPATH" in l:
        picklestorepath = l.strip().split("=")[1]

print("\n  update_batches.py = = = = = " + time.ctime())

# LOADING BATCH
#===============

bopath = os.path.abspath(os.path.join(picklestorepath, "batch_organizer.pkl"))
print(bopath,os.path.isfile(bopath))
with open(bopath, "rb") as f:
    bo = pickle.load(f)

# UPDATING BATCH
#===============

bo.terminate_step()
print(bo)
