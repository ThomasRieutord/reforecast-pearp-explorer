#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Tools to access the long-term PEARP reforecast made at CNRM/GMAP/RECYF.
Define batches of transferts between machines.

@author: Rieutord Thomas
@date: 2022/05/02


Purpose
-------
This program defines the batches that will be used in a batch transfert of PEARP reforecast data.

The data to be transfered is identified by a **set of dates, members, leadtimes and domains**.
The data is originated from Hendrix, the High Performance Storage System of Meteo-France.
It will be transfered to a **final host** (which must have enough space to store the full dataset.
By default, only the selected fields are extracted and the final format is netCDF4.
The extraction from Hendrix and the conversion to netCDF is made on an **execution host** (which must have granted access to both hendrix and the final host).
The **batch size** must be chosen according to the storage capacity of the execution host.

Are highlighted in the text the main elements to set up the batch transfert, namely:
  * a set of dates, members, leadtimes and domains
  * a final host
  * an execution host
  * a batch size


For more documentation, please read ./how_to_make_batch_transfert.md


Parameters
----------
rundate_start: `datetime.datetime`
    The starting date of the total time period to be transfered

rundate_end: `datetime.datetime`
    The ending date of the total time period to be transfered

prefixdir: str
    Directory name to distinguish several purpose of use

domains: {"eurat01", "glob025", ["eurat01", "glob025"]}
    List of domains to be transfered.
    Default is ["eurat01", "glob025"] (it means both "eurat01" and "glob025" domains)

members: list of int
    List of members to be transfered.

hleadtimes: list of int
    List of leadtimes to be transfered, in hours.

removegribs: bool
    If True, the GRIB files are removed after the extraction into netCDF

exechost: str
    The name of the execution machine (where extraction scripts are run and temporary batches are stored)
    Default is the machine on which this program is launched (os.uname()[1])

finalhost: str
    The name of the final machine (where the final data will be stored)


Returns
-------
The files to define and to track the batches are created in the repository `picklestorepath`
The transferts are ready to begin. To launch them, follow the next steps.


Example
-------
>> # To launch this program, use the following command in a terminal
>> # This example was done with the parameters
>> # rundate_start =  dt.datetime(2018,9,24,0)
>> # rundate_end =  dt.datetime(2018,10,26,0)
>> # prefixdir = "mf2laero_test"
>> # domains = ["eurat01", "glob025"]
>> # members = [0,7]
>> # hleadtimes = [3,6]
>> # removegribs = True
>> # batch_size = 2
>> # exechost = os.uname()[1]
>> # finalhost = "sxrecyf2.cnrm.meteo.fr"
>>
>> python batches_definitions.py
Creating batches for rundates from  2018-09-24 00:00:00 to 2018-10-26 00:00:00
Batch 1/7 of size 2
Batch 2/7 of size 2
Batch 3/7 of size 2
Batch 4/7 of size 2
Batch 5/7 of size 2
Batch 6/7 of size 2
Batch 7/7 of size 1
All batches created at /scratch/mtool/rieutordt/cache/usertmp/reforecast-pearp-cy46t1/toolsync
User executing the final transfert must execute: cat /home/gmap/mrmn/rieutordt/.rpe_hosts_and_paths > ~/.rpe_hosts_and_paths


Next steps
----------
After the execution of this program, the transferts are ready to begin.
The next steps are:

  * Copy the file .rpe_hosts_and_paths
    The user executing the final transfert must execute the command provided at the end of the output of `python batches_definitions.py`

  * Set up the crons
    Execute `crontab -e` then copy-paste from the file named `croncmd_<exechost>` with `<exechost>` being the execution host
"""

import sys
import os
import time
import numpy as np
import datetime as dt
import pickle

from reforecast_pearp_explorer import batching
from reforecast_pearp_explorer.settings import ReforecastSettings

rfs = ReforecastSettings()

#============================================
# >>> START OF THE VARIABLE PART
#============================================

# Parameters
#------------

### Full reforecast
# rundate_start = rfs.veryfirstdate
# rundate_end = rfs.verylastdate
members = [i for i in range(rfs.n_members)]
leadtimes = rfs.leadtimes

### Test subset
rundate_start =  dt.datetime(2020,9,12,0)
rundate_end =  dt.datetime(2021,9,18,0)
domains = ["eurat01", "glob025"]
#members = [0,7]
#hleadtimes = [3,6]
removegribs = True
batch_size = 10

#============================================
# <<< END OF THE VARIABLE PART
#============================================


# Deductions from parameters
#---------------------------

with open(os.path.expanduser("~/.rpe_hosts_and_paths"), "r") as f:
    lines = f.readlines()
for l in lines:
    if "PREFIXDIR" in l:
        prefixdir = l.strip().split("=")[1]
    if "PICKLESTOREPATH" in l:
        picklestorepath = l.strip().split("=")[1]
    if "MIDDLESTOREPATH" in l:
        middlestorepath = l.strip().split("=")[1]
    if "TARGETHOST" in l:
        targethost = l.strip().split("=")[1]

rundates = rfs.get_rundates_between_dates(rundate_start, rundate_end)
#leadtimes = [dt.timedelta(hours=h) for h in hleadtimes]

print("Creating batches for rundates from ", rundate_start, "to", rundate_end)

# Create batches
#---------------

### Define batches
batches_rds = batching.create_batches(rundates, batch_size=batch_size)

### Instanciate batches
batches = []
for batch, rds in enumerate(batches_rds):
    print(f"Batch {batch+1}/{len(batches_rds)} of size {len(rds)}")
    rfb = batching.ReforecastBatch(batch, rds, members=members, leadtimes=leadtimes, domains=domains)
    batches.append(rfb)
    
### Instanciate batch loader
bo = batching.BatchLoader(
    initial_batches = batches,
    picklestorepath = picklestorepath,
    middlestorepath = middlestorepath,
    prefixdir = prefixdir,
    removegribs = removegribs,
)

### Save into Pickle file
bo.create_picklestore()

print("All batches created at " + picklestorepath)


msg = f"""
  --- Next steps ---
 (4/5) Set up the crons: copy the output of `cat crontab_command.txt` and paste it into the editor of `crontab -e`
 (5/5) Check advancement: `python check_batch_loader.py`
"""

print(msg)
