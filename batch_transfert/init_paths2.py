#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Tools to access the long-term PEARP reforecast made at CNRM/GMAP/RECYF.
Initialize paths (create useful directories and check it) before batch transfert of reforecast data.

@author: Rieutord Thomas
@date: 2022/07/12

Purpose
-------
Set up the directories that will be used in the batch transfert and create them if they don't exist.
It must be executed on both the execution host and the final host (see ./how_to_make_batch_transfert.md for definitions of these names).


Parameters
----------
prefixdir: str
    Directory name to distinguish several purpose of use

role: {'source2middle', 'middle2target'}
    Role of the current host. Only two roles are possible:
        'source2middle' for the host making the transfert from the source host to the middle host
        'middle2target' for the host making the transfert from the middle host to the target host

thirdarg: str
    Depending on the given `role`, this argument is interpreted differently.
    If role=='source2middle', thirdarg is an optional argument to trigger cleaning.
    If role=='middle2target', thirdarg is a mandatory argument to specify the target host.
    
    
Examples
--------
Let say we want to make the tranfert from the source host to the middle host on *sxrecyf*
and the transfert from the middle host to the target host on *sxphynh*.
The target host is specified as for an SSH connection, let say 'user@target.host.fr'.
The directory in which will be stored the data is called 'prefixdirtest'.

On sxrecyf
```
python init_paths2.py prefixdirtest source2middle
```

On sxphynh
```
python init_paths2.py prefixdirtest middle2target user@target.host.fr
```

If we want to clean the directory containing the batches, on sxrecyf, we would run
```
python init_paths2.py prefixdirtest source2middle true
```
"""
import shutil
import os
import sys
from reforecast_pearp_explorer.settings import ReforecastSettings

rfs = ReforecastSettings()

# Parse arguments
#-----------------

### prefixdir 
try:
    prefixdir = sys.argv[1]
except IndexError:
    raise IndexError("The argument 'prefixdir' must be provided")

### role 
try:
    role = sys.argv[2]
except IndexError:
    raise IndexError("The argument 'role' must be provided")

if role not in ['source2middle', 'middle2target']:
    raise ValueError("Invalid role. Please choose among ['source2middle', 'middle2target']")

### thirdarg (cleanup | targethost)
# Adresse NUWA: panf@nuwa.aero.obs-mip.fr
try:
    thirdarg = sys.argv[3]
except IndexError:
    thirdarg = None

if role=='source2middle':
    cleanup = thirdarg in ["T", "True", "true", "TRUE"]
    targethost = None
else:
    targethost = thirdarg
    cleanup = False
    if targethost is None:
        raise ValueError("The targethost must be provided when role="+role)

print("Parsed arguments: prefixdir=", prefixdir, "role=", role, "cleanup=", cleanup, "targethost=", targethost)

# Initialize paths
#------------------
middlehost = os.uname()[1]

scriptspath = os.getcwd()
condapath = os.path.join(
    # Solve issue "CommandNotFoundError: Your shell has not been properly configured to use 'conda activate'"
    # Source (2022-07-11); https://github.com/conda/conda/issues/7980
    os.popen("conda info | grep -i 'base environment'").read().strip().split(' ')[3],
    "etc/profile.d/conda.sh"
)

middlebasepath = rfs.get_localbasepath(middlehost)
middlestorepath = os.path.join(middlebasepath, prefixdir)
picklestorepath = os.path.join(middlebasepath, "toolsync")

to_export = ["prefixdir", "scriptspath", "condapath", "picklestorepath", "middlehost", "middlestorepath"]


if targethost is not None:
    targetbasepath = rfs.get_localbasepath(targethost)
    targetstorepath = os.path.join(targetbasepath, prefixdir)
    to_export += ["targethost", "targetstorepath"]


# Export environnement variables
#------------------
with open(os.path.expanduser("~/.rpe_hosts_and_paths"),"w") as f:
    for v in to_export:
        f.write(v.upper()+"="+str(vars()[v])+"\n")


# Create directories
#------------------

if not os.path.isdir(middlestorepath):
    os.makedirs(middlestorepath)
    print("Create directory", middlestorepath)
if not os.path.isdir(picklestorepath):
    os.makedirs(picklestorepath)
    os.chmod(picklestorepath, 0o777)
    print("Create directory", picklestorepath)
else:
    if cleanup:
        os.system("rm -f " + picklestorepath + "/*")
        print("Cleaning directory", picklestorepath)

if targethost is not None:
    # Source (2022/07/12): https://unix.stackexchange.com/questions/8612/programmatically-creating-a-remote-directory-using-ssh
    sshcmd = "ssh " + targethost + " 'test -d " + targetstorepath + " || mkdir " + targetstorepath + "'"
    print("Executing", sshcmd)

logfile = os.path.join(picklestorepath, "transferts.log")
if not os.path.isfile(logfile):
    with open(logfile, "w") as f:
        f.write("Log file init - PREFIXDIR="+prefixdir+"\n")
    os.chmod(logfile, 0o666)

with open(logfile, "a") as f:
    f.write("   " + role + " on "+middlehost)
    f.write("   " + role + ".rpe_hosts_and_paths\n")
    for v in to_export:
        f.write(v.upper()+"="+str(vars()[v])+"\n")

# Check paths
#-------------

### Consistency between the two roles

shutil.copy(os.path.expanduser("~/.rpe_hosts_and_paths"), os.path.join(picklestorepath, role+".rpe_hosts_and_paths"))

if role=='source2middle':
    altrole = "middle2target"
else:
    altrole = "source2middle"

altfile = os.path.join(picklestorepath, altrole+".rpe_hosts_and_paths")
if os.path.isfile(altfile):
    with open(altfile, "r") as f:
        lines = f.readlines()
    for l in lines:
        if "PREFIXDIR" in l:
            altprefixdir = l.strip().split("=")[1]
            if altprefixdir != prefixdir:
                raise ValueError("Incoherent values for PREFIXDIR")
        if "PICKLESTOREPATH" in l:
            altpicklestorepath = l.strip().split("=")[1]
            if altpicklestorepath != picklestorepath:
                raise ValueError("Incoherent values for PICKLESTOREPATH")
        if "MIDDLESTOREPATH" in l:
            altmiddlestorepath = l.strip().split("=")[1]
            if altmiddlestorepath != middlestorepath:
                raise ValueError("Incoherent values for MIDDLESTOREPATH")
        
    print("Check consistency between role "+role+" and "+altrole+": OK.")

### Existence of paths

to_check = {
    "scriptspath":scriptspath,
    "condapath":condapath,
    "picklestorepath":picklestorepath,
    "middlestorepath":middlestorepath,
}

misscount = 0
for path in to_check.keys():
    if not os.path.exists(to_check[path]):
        print("Not existing or unchecked -> "+middlehost+":"+to_check[path])
        misscount += 1

if misscount==0:
    print("All paths are correctly set!")
else:
    print(misscount,"paths are not existing or unchecked")

# Outro
#-------
if role=="source2middle":
    how_to_define_batches = "`python batches_definition.py`"
else:
    how_to_define_batches = "done by the user with role=source2middle (you have nothing to do)"

crontab_command = "* * * * * " + os.path.join(scriptspath,"trigger_" + role + ".sh")
with open("crontab_command.txt", "w") as f:
    f.write(crontab_command)

msg = f"""
  --- Next steps ---
 (3/5) Define the batches: {how_to_define_batches}
 (4/5) Set up the crons: copy the following line and paste it into the editor of `crontab -e`:
 {crontab_command}
 (5/5) Check advancement: `python check_batch_loader.py`
"""

print(msg)
