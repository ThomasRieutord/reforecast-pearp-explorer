#!/home/rieutordt/anaconda3/envs/rpe/bin/python
# -*- coding: utf-8 -*-
"""Simple progression bar with chrono"""
print("transfert_hendrix2exec")
import os
import sys
import time
import pickle
import vortex
import common
import numpy as np
from reforecast_pearp_explorer import access
from reforecast_pearp_explorer import gribs
from reforecast_pearp_explorer.settings import ReforecastSettings

rfs = ReforecastSettings()

with open(os.path.expanduser("~/.rpe_hosts_and_paths"), "r") as f:
    lines = f.readlines()
for l in lines:
    if "PICKLESTOREPATH" in l:
        picklestorepath = l.strip().split("=")[1]
print("\n  transfert_hendrix2exec = = = = = = " + time.ctime())

# LOADING BATCH
#===============

bopath = os.path.abspath(os.path.join(picklestorepath, "batch_organizer.pkl"))
print(bopath,os.path.isfile(bopath))
with open(bopath, "rb") as f:
    bo = pickle.load(f)

rfb = bo.get_current_batch()

print(rfb)


# EXECUTING TRANSFERT
#===============

# The session's ticket
t = vortex.ticket()
vortex.toolbox.active_now = True

# Parameters
#------------

prefixdir = bo.prefixdir
removegribs = bo.removegribs

rundates = rfb.rundates
leadtimes = rfb.leadtimes
members = rfb.members
domains = rfb.domains


# wkdir = t.sh.path.join( t.sh.path.abspath(tmpdir),"wkdir_{:d}".format(t.sh.getpid()))
wkdir = t.env.MTOOLDIR
t.sh.cd(wkdir, create = True)
print('The temporary working directory is: {}'.format(wkdir))
t.env.FTDIR = wkdir
t.env.WORKDIR = wkdir

rhandlers = access.get_rhandlers(t, rundates, leadtimes, members, domains, quiet=True)

print(len(rhandlers), "rhandlers created")
# ### Prestaging
# for rh in rhandlers:
    # rh.quickview()
    # print('Does it exists ?', rh.check())
    # rh.prestage()


for i,rh in enumerate(rhandlers):
    
    
    hdxgribpath = t.sh.path.join(access.hendrixbasepath, rh.provider.pathname(rh.resource), rh.provider.basename(rh.resource))
    gribtmpfile = rh.container.abspath
    loctrgdir = access.vortexpath_to_lustrepath(hdxgribpath, prefixdir=prefixdir, withfilename=False)
    locncpath = t.sh.path.join(
        loctrgdir,
        rh.provider.basename(rh.resource)[:-5]+".nc"
    )
    print(f" - - - - - - - - {i}/{len(rhandlers)}")
    print("Hendrix GRIB path",hdxgribpath)
    print("Local tmp GRIB path", gribtmpfile)
    print("Local netCDF path",locncpath)
    
    t.sh.mkdir(loctrgdir)
    
    ### Download
    print("Download grib...")
    rh.get()
    
    ### Extract
    print("Extract and convert..")
    borders = rfs.borders_for_extraction[rh.resource.geometry.tag]
    gribs.drop_to_netcdf(gribtmpfile, locncpath, subdom_borders=borders, dtype = rfb.dtype)
    if removegribs:
        t.sh.rm(gribtmpfile)
        print("rm GRIB file ", gribtmpfile)
    

# UPDATING BATCH
#===============

bo.terminate_step()
print(bo)


