#!/home/rieutordt/anaconda3/envs/rpe/bin/python
# -*- coding: utf-8 -*-
"""Simple progression bar with chrono"""

import os
import sys
import time
import pickle

# tmpdir = "../tmp/synchro"
# picklestorepath = "/cnrm/recyf/NO_SAVE/Data/users/rieutordt/reforecast-pearp-cy46t1/toolsync"
picklestorepath = "/home/rieutordt/Travaux/Reforecast-explorer/gitsync/reforecast-pearp-explorer/tmp/toolsync"
print("\n===================================================== " + time.ctime())
# LOADING BATCH
#===============
# os.remove(os.path.join(tmpdir, "go_to_next_batch.txt")) # Done in the shell script
bopath = os.path.abspath(os.path.join(picklestorepath, "batch_organizer.pkl"))
# bopath = os.path.join(picklestorepath, "batch_organizer.pkl")
print(bopath,os.path.isfile(bopath))
with open(bopath, "rb") as f:
    bo = pickle.load(f)

# batch_number = bo.current_batch_hendrix2belenos
rfb = bo.get_current_batch()

# print(bo)
print(rfb)

# with open(os.path.join(tmpdir, f"batch_{batch_number}.pkl"), "rb") as f:
    # rfb = pickle.load(f)

# if bo.current_step != "hendrix2belenos":
    # # raise Exception("This batch seems to have already been treated")
    # raise Exception("This batch seems to have already been treated")

# EXECUTING TRANSFERT
#===============

toolbar_width = 10

# setup toolbar
t0 = time.time()  #::::::::::::::::::::::
sys.stdout.write(
    "Toolbar: [%s]" % ("." * toolbar_width)
)
sys.stdout.flush()
sys.stdout.write("\b" * (toolbar_width + 1))  # return to start of line, after '['

for i in range(toolbar_width):
    time.sleep(0.1)
    
    # core toolbar
    sys.stdout.write("*")
    sys.stdout.flush()
    
# end toolbar
t1 = time.time()  #::::::::::::::::::::::
chrono = int((t1 - t0)*100)/100.0
sys.stdout.write("] (" + str(chrono) + " s)\n")

# UPDATING BATCH
#===============

# rfb.hendrix2belenos_done = True
# rfb.update_pickle_file(bo.path_to_picklestore)
bo.terminate_step()
print(bo)


