#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Tools to access the long-term PEARP reforecast made at CNRM/GMAP/RECYF.
Initialize paths (create useful directories and check it) before batch transfert of reforecast data.

@author: Rieutord Thomas
@date: 2022/06/10

Purpose
-------
Look for the directories that will be used in the batch transfert and create them if they don't exist.
It must be executed on both the execution host and the final host (see ./how_to_make_batch_transfert.md for definitions of these names).


Usage
-----
On the final host
```
python init_paths.py <prefixdir>
```

On the execution host
```
python init_paths.py <prefixdir> true
```
"""
import os
import sys
from reforecast_pearp_explorer.settings import ReforecastSettings

rfs = ReforecastSettings()

# Parse arguments
#-----------------
try:
    prefixdir = sys.argv[1]
except IndexError:
    raise IndexError("The argument 'prefixdir' must be provided")

try:
    isexec = sys.argv[2] in ["T", "True", "true"]
except IndexError:
    isexec = False

# Initialize paths
#------------------
hostname = os.uname()[1]

localbasepath = rfs.get_localbasepath(hostname)
datastorepath = os.path.join(localbasepath, prefixdir)
if not os.path.isdir(localbasepath):
    os.makedirs(localbasepath)
    print("Create directory", localbasepath)
if not os.path.isdir(datastorepath):
    os.makedirs(datastorepath)
    print("Create directory", datastorepath)

if isexec:
    picklestorepath = os.path.join(localbasepath, "toolsync")
    if not os.path.isdir(picklestorepath):
        os.mkdir(picklestorepath)
        print("Create directory", picklestorepath)
    else:
        os.system("rm -f " + picklestorepath + "/*")
        print("Cleaning directory", picklestorepath)
else:
    picklestorepath = os.path.expanduser("~")


# Check paths
#-------------
tocheck = {
    "localbasepath":localbasepath,
    "picklestorepath":picklestorepath,
    "datastorepath":datastorepath,
}

misscount = 0
for path in tocheck.keys():
    if not os.path.exists(tocheck[path]):
        print("Not existing or unchecked -> "+hostname+":"+tocheck[path])
        misscount += 1

if misscount==0:
    print("All paths are correctly set!")
else:
    print(misscount,"paths are not existing or unchecked")


