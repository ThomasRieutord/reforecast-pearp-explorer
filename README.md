RPE : Reforecast PEARP Explorer
==============================


What is RPE?
--------------
RPE is a toolbox to facilitate the use of the PEARP long-range reforecast.

How to use it?
---------------
The RPE package can be installed by following the **installation instructions below**.
However, the package is intented to be used inside Météo-France infrastructure.
Therefore, the use of the package on other infrastructure is not guaranteed and might be difficult.

Once installed, you will find ready-to-use scripts in the `examples` directory.


Contacts
---------
This software is edited by Meteo-France.
  * Thomas Rieutord (CNRM) : thomas.rieutord@meteo.fr

Licence
--------
This software is under [CeCILL](https://cecill.info/licences.en.html) licence (open source, French equivalent to GNU/GPL licence).
Terms are in the Licence_CeCILL_V2.1-en.txt file.


Installation
=============

Required environment
--------------------
The package is intented to be used inside Météo-France infrastructure.
The following assumption are made:
  * the installation is done on a Météo-France computer,
  * the computer has access to the LUSTRE facility through a NFS connexion,
  * the computer has access to the Hendrix facility through a FTP connexion,
  * the Python packages `epygram`, `olive` and `vortex`, edited by Meteo-France, are correctly installed,
  * the Shell functions `dirh`, `ftget`, `ftput`, edited by Meteo-France, are correctly installed.
  
Please refer to the administrator of your computer to ensure these criteria are met.

Installing dependencies
------------------------

Conda is a package manager very helpful in Python.
If you do not have `conda`, you can download it [here](https://docs.conda.io/projects/conda/en/latest/index.html).

The advantage of using `conda` the way described here is to isolate the python configuration needed for RPE from the rest your current configuration.

```bash
conda  env create -f rpe_env.yml
conda activate rpe
```

Installing EPyGrAM
------------------
Please follow the installation instructions as found following this link : [https://opensource.umr-cnrm.fr/projects/epygram/wiki/Installation]

Installing the package
-----------------------
This can be done only with `pip`.
In the directory where is the `setup.py`, run:
```bash
pip install -e .
```

The package
============

Description
---------------

The RPE package, when cloned, shows several directories with different use.
  * `batch_transfert`: programs specific to the transfert of data with batches
  * `examples`: off-the-shelf programs making use of the RPE package
  * `reforecast_pearp_explorer`: the core of RPE package. Contains only classes and functions. No executable code
  * `tmp`: receive all non-final files created during the executions

The core of the RPE package is located in the `reforecast_pearp_explorer` directory. It contains several Python source codes :
  * `access.py`: module containing the functions to access and download the data from Hendrix.
  * `batching.py`: module containing the functions to manipulate batches of data.
  * `graphics.py`: module containing all graphical functions and settings.
  * `gribs.py`: module containing useful functions to manipulate gribs (mostly a reforecast-coating of Epygram).
  * `settings.py`: module containing all data and methods describing the reforecast.
  * `vtxutils.py`: module containing functions using the `vortex` or `olive` package (to be deprecated).

Use the package
---------------
Once the installation is ready (prompt should display `(rpe)` if you are using conda), you can execute the ready-to-use script in the `examples` directory:
```bash
python prestage_only.py
```


