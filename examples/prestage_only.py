#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Tools to access the long-term PEARP reforecast made at CNRM/GMAP/RECYF.
Create a prestaging file and send the resquest (or don't).

@author: Rieutord Thomas
@date: 2021/09/09
"""
import os
import time
import datetime as dt
from reforecast_pearp_explorer import access
from reforecast_pearp_explorer.settings import ReforecastSettings

rfs = ReforecastSettings()

# rundate_start =  dt.datetime(2011,11,3,0)
# rundate_end =  dt.datetime(2011,11,7,0)
# rundate_start =  dt.datetime(2018,9,24,0)
# rundate_end =  dt.datetime(2019,3,28,0)
rundate_start =  dt.datetime(2020, 9, 12, 12)
rundate_end =  dt.datetime(2021, 9, 18, 0)
rundates = rfs.get_rundates_between_dates(rundate_start, rundate_end)

tmpdir = "../tmp/prestaging/"
# prefixdir = "cyclone-ianos"
domain="glob025"

prstgfile = access.prestage(
    # rundate_start,
    # rundate_end,
    rundates,
    domain=domain,   # eurat01, glob025
    submit=True,
    tmpdir=tmpdir,
    doublecheck=False
)
