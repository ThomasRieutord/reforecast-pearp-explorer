#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Tools to access the long-term PEARP reforecast made at CNRM/GMAP/RECYF.
Test conversion from GRIB to netCDF with different options (e.g. compression with coarser dtype).

@author: Rieutord Thomas
@creationdate: 2022/01/12
"""

import os
import numpy as np
import datetime as dt
import epygram
from reforecast_pearp_explorer import gribs
from reforecast_pearp_explorer import access
from reforecast_pearp_explorer.settings import ReforecastSettings

rfs = ReforecastSettings().build()


testbasepath = "/cnrm/recyf/NO_SAVE/Data/users/rieutordt/test/GCQ7.7"
isgrib = lambda p: p.split(".")[-1]=="grib"
lgribs = list(filter(isgrib,os.listdir(testbasepath)))
lgribpath = [os.path.join(testbasepath,gb) for gb in lgribs]
# lgribpath = ["/cnrm/recyf/NO_SAVE/Data/users/rieutordt/test/GCQ7.1/grid.arpege-forecast.glob025+0009:00.grib"]


for gribpath in lgribpath:
    print("\nPath to grib:", gribpath)
    
    domain = access.get_domain_from_filename(gribpath)
    
    fields_to_export = rfs.fields_to_extract[domain]
    borders = rfs.borders_for_extraction[domain]
    
    netcdfpath = gribpath[:-5] + ".nc"
    gribs.drop_to_netcdf(gribpath, netcdfpath=netcdfpath, reqfields=fields_to_export, subdom_borders=borders)
    netcdfpath = gribpath[:-5] + ".f32.nc"
    gribs.drop_to_netcdf(gribpath, netcdfpath=netcdfpath, reqfields=fields_to_export, subdom_borders=borders, dtype=np.float32)
    netcdfpath = gribpath[:-5] + ".f16.nc"
    gribs.drop_to_netcdf(gribpath, netcdfpath=netcdfpath, reqfields=fields_to_export, subdom_borders=borders, dtype=np.float16)
    
# Debug field handgrips
#-----------------------
go = epygram.formats.resource(gribpath, openmode="r")
hg = {"discipline":0, "parameterCategory":0, "parameterNumber":27}
hg = {"discipline":0, "parameterCategory":3, "parameterNumber":6, "typeOfFirstFixedSurface":198, "scaledValueOfFirstFixedSurface":27315}
hg = {"discipline":0, "parameterCategory":0, "parameterNumber":3, "typeOfFirstFixedSurface":100}
n_matches = gribs.handgrip_matches(hg, go)
print(n_matches, "fields match with the handgrip", hg)
[print(f) for f in go.listfields(select=hg)]
gribs.display_fields_params(go, sel=hg, keys = ["discipline", "parameterCategory", "parameterNumber", "shortName", "name", "typeOfStatisticalProcessing", "level"])
