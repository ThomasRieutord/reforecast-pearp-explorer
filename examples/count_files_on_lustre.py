#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Tools to access the long-term PEARP reforecast made at CNRM/GMAP/RECYF.
Check if files exist in the local machine (LUSTRE)

@author: Rieutord Thomas
@date: 2021/09/06
"""

import os
import time
import datetime as dt
from reforecast_pearp_explorer import graphics
from reforecast_pearp_explorer import access

rundate_start =  dt.datetime(2010,1,1,18)
rundate_end =  dt.datetime(2010,5,31,18)
domain = "eurat01"
prefixdir = "full-float16-003"

figname = "barplot_fileexist_fullf16_"+domain

graphics.storeImages = True
graphics.figureDir = "../figures"

rd, oc, ec = access.count_files_per_rundate(rundate_start, rundate_end, domain=domain, ldtdex=[1], prefixdir=prefixdir, fileformat="nc")
print("Prefix dir:", prefixdir,"Observed count/Expected count ratio:", sum(oc)/sum(ec)*100, "%")
graphics.barplot_fileexist(rd, oc, ec, groupby="month", figname=figname)

# # Prepare re-download
# #---------------------

# access.prepare_redownload(xpid, domain, tmpdir="../tmp")
