#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Tools to access the long-term PEARP reforecast made at CNRM/GMAP/RECYF.
Upload files that are present in the local machine (LUSTRE) onto a remote FTP server.

@author: Rieutord Thomas
@date: 2021/09/06
"""

import os
import numpy as np
import datetime as dt
from reforecast_pearp_explorer import access

rundate_start =  dt.datetime(2016,12,31,18)
rundate_end =  dt.datetime(2017,2,1,0)
rundate_start =  dt.datetime(2020, 9, 12, 12)
rundate_end =  dt.datetime(2020, 9, 18, 0)
domain = "eurat01"
prefixdir = "cyclone-ianos"
leadtimes = "all" #[dt.timedelta(hours=h) for h in [3,24,48,63,84,108]]
submit=True
doublecheck=True

cmd_history = access.upload_to_ftpserver(
    rundate_start,
    rundate_end,
    domain=domain,
    leadtimes=leadtimes,
    prefixdir=prefixdir,
    submit=submit,
    doublecheck=doublecheck,
)

