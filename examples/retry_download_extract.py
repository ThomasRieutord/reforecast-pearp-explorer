#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Tools to access the long-term PEARP reforecast made at CNRM/GMAP/RECYF.
Prestaging + download + extract

@author: Rieutord Thomas
@date: 2022/03/08
"""

import os
import time
import numpy as np
import datetime as dt
from reforecast_pearp_explorer import access
from reforecast_pearp_explorer import gribs
from reforecast_pearp_explorer.settings import ReforecastSettings

rfs = ReforecastSettings().build()

ftgetfile = "../tmp/prestaging/retry_200003-202002_3hbj85_0.txt"
tmpdir = "../tmp/prestaging"
prefixdir = "full-float16-003"
domain="eurat01"
borders = None #{"lonmin":-75, "latmin":20, "lonmax":55, "latmax":70}
removegribs = True
reqfields = rfs.fields_to_extract[domain]

with open(ftgetfile, 'r') as f_in:
    lines = f_in.readlines()

missing = []
for l in lines:
    src_file, trg_file = l.strip().split(" ")
    
    if os.path.isfile(trg_file[:-5] + ".nc"):
        print("File " + trg_file[:-5] + ".nc already exists")
        continue
    
    cmd_ftget = "ftget -h hendrix -u rieutordt "+src_file+" "+trg_file
    
    ### Download
    os.system(cmd_ftget)
    
    ### Extract
    if os.path.isfile(trg_file):
        gribs.drop_to_netcdf(trg_file,reqfields=reqfields, subdom_borders=borders, dtype = np.float16)
    else:
        print("Missing file ", trg_file, "-- Added to the re-try file")
        missing.append(src_file + " " + trg_file)
    
    if removegribs:
        os.system("rm -f "+trg_file)
        print("rm GRIB file ", trg_file)
    

h,t = os.path.split(ftgetfile)
retryfile = os.path.join(h, "retry"+t[-27:])

with open(retryfile, 'w') as f_out:
    for m in missing:
        f_out.write(m + "\n")

print(len(missing), "missing files. Re-try download with the file ",retryfile)
