#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Tools to access the long-term PEARP reforecast made at CNRM/GMAP/RECYF.
Prestaging + download

@author: Rieutord Thomas
@date: 2021/09/06
"""

import os
import time
import datetime as dt
from reforecast_pearp_explorer import access

rundate_start =  dt.datetime(2018,3,1,18)
rundate_end =  dt.datetime(2018,4,14,18)
tmpdir = "../tmp/prestaging"
prefixdir = "smallsample"

prstgfile = access.prestage(
    rundate_start,
    rundate_end,
    domain="glob025",   # eurat01, glob025
    submit=True,
    tmpdir=tmpdir,
    doublecheck=True
)

input("Press enter when the prestaging is done (email)")

access.ftpdownload(prstgfile, submit=True, prefixdir=prefixdir, tmpdir=tmpdir)
