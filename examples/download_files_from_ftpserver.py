#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Tools to access the long-term PEARP reforecast made at CNRM/GMAP/RECYF.
Download files from a remote FTP server (not Hendrix).

@author: Rieutord Thomas
@date: 2022/03/14
"""

import os

# Change this path to put the landing path of the files on the local host
local_host_basepath="/cnrm/recyf/NO_SAVE/Data/users/rieutordt/reforecast-pearp-cy46t1/downloadtest"
# Change this path to put the source path of the files on the remote host
ftp_host_basepath = "/reforecast-cy46t1/jan2017sample"

ftp_host="ftp.umr-cnrm.fr"
ftp_login="eunadics"
ftp_pwd = "18pl@smA20"

lftp_cmd = "lftp -u "+ftp_login+","+ftp_pwd+' -e "mirror '+ftp_host_basepath+' '+local_host_basepath+';quit" ftp://'+ftp_host

print(lftp_cmd)
answer = input("Continue ? (yes/no)")
if answer.lower() in ['n','no','non']:
    exit("Execution stopped by the user")
else:
    os.system(lftp_cmd)

# End of file

