#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Tools to access the long-term PEARP reforecast made at CNRM/GMAP/RECYF.
Simple program to quickly plot fields.

@author: Rieutord Thomas
@creationdate: 2021/12/13
"""

import os
import time
import datetime as dt
import numpy as np
import epygram
from reforecast_pearp_explorer import graphics
from reforecast_pearp_explorer import gribs
from reforecast_pearp_explorer.settings import ReforecastSettings

import metpy.calc as mpcalc
from metpy.units import units

rfs = ReforecastSettings().build()

gp = "/cnrm/recyf/NO_SAVE/Data/users/rieutordt/test/GCQ7.6/grid.arpege-forecast.eurat01+0000:00.grib"
gp = "/cnrm/recyf/NO_SAVE/Data/users/rieutordt/reforecast-pearp-cy46t1/jan2017sample/20170113T0600P/mb005/grid.arpege-forecast.glob025+0090:00.grib"
gp = "/cnrm/recyf/NO_SAVE/Data/users/rieutordt/reforecast-pearp-cy46t1/full-float16-003/20080301T1800P/mb004/grid.arpege-forecast.eurat01+0003:00.grib"
print("Path to grib:", gp)

graphics.figureDir = "../figures/plot_fields_simple/"
graphics.storeImages = True

domain = gribs.get_domain_from_filename(gp)
fields_to_check = rfs.fields_to_extract[domain]

fields_to_handgrip = rfs.all_field2gribid

go = epygram.formats.resource(gp, openmode="r")

for fd in fields_to_check:
    field=gribs.get_field(go,fd)
    # graphics.plot_field(field)

# Debug field handgrips
#-----------------------

hg = {'parameterNumber': 12}
n_matches = gribs.handgrip_matches(hg, go)
print(n_matches, "fields match with the handgrip", hg)
[print(f) for f in go.listfields(select=hg)]
gribs.display_fields_params(go, sel=hg, keys = ["parameterNumber", "shortName", "name", "lengthOfTimeRange", "level"])
