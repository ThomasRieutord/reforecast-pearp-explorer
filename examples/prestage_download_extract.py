#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Tools to access the long-term PEARP reforecast made at CNRM/GMAP/RECYF.
Prestaging + download + extract

@author: Rieutord Thomas
@date: 2022/03/08
"""

import os
import time
import numpy as np
import datetime as dt
from reforecast_pearp_explorer import access
from reforecast_pearp_explorer import gribs
from reforecast_pearp_explorer.settings import ReforecastSettings

rfs = ReforecastSettings()

# Parameters
#------------
# rundate_start =  dt.datetime(2011,11,3,0)
# rundate_end =  dt.datetime(2011,11,7,0)
# rundate_start =  dt.datetime(2018,9,24,0)
# rundate_end =  dt.datetime(2018,9,28,0)
rundate_start =  dt.datetime(2020, 9, 12, 12)
rundate_end =  dt.datetime(2020, 9, 18, 0)
tmpdir = "../tmp/prestaging"
prefixdir = "cyclone-ianos"
domain="eurat01"
borders = rfs.borders_for_extraction[domain]
removegribs = True
reqfields = rfs.fields_to_extract[domain]

# years = np.arange(2000,2020,2)
# months = np.arange(3,13)
# rundates = rfs.get_rundates_between_dates(rfs.veryfirstdate, rfs.verylastdate, years = years, months = months)
rundates = rfs.get_rundates_between_dates(rundate_start, rundate_end)

### Prestaging
prstgfile = access.prestage(
    # rundate_start,
    # rundate_end,
    rundates,
    leadtimes = "all",
    domain=domain,   # eurat01, glob025
    submit=False,
    tmpdir=tmpdir,
    doublecheck=False
)

input("Press enter when the prestaging is done (email)")

ftgetfile = access.ftpdownload(prstgfile, submit=False, prefixdir=prefixdir, tmpdir=tmpdir)

with open(ftgetfile, 'r') as f_in:
    lines = f_in.readlines()

missing = []
for l in lines:
    src_file, trg_file = l.strip().split(" ")
    
    if os.path.isfile(trg_file[:-5] + ".nc"):
        print("File " + trg_file[:-5] + ".nc already exists")
        continue
    
    cmd_ftget = "ftget -h hendrix -u rieutordt "+src_file+" "+trg_file
    
    ### Download
    os.system(cmd_ftget)
    
    ### Extract
    if os.path.isfile(trg_file):
        gribs.drop_to_netcdf(trg_file,reqfields=reqfields, subdom_borders=borders, dtype = np.float64)
    else:
        print("Missing file ", trg_file, "-- Added to the re-try file")
        missing.append(src_file + " " + trg_file)
    
    if removegribs:
        os.system("rm -f "+trg_file)
        print("rm GRIB file ", trg_file)
    

h,t = os.path.split(ftgetfile)
retryfile = os.path.join(h, "retry"+t[-27:])

with open(retryfile, 'w') as f_out:
    for m in missing:
        f_out.write(m + "\n")

print(len(missing), "missing files. Re-try download with the file ",retryfile)
