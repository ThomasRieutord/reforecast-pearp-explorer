#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Tools to access the long-term PEARP reforecast made at CNRM/GMAP/RECYF.
Short program to estimate the volume storage.

@author: Rieutord Thomas (thomas.rieutord@meteo.fr)
@institution: Météo-France DESR/CNRM/GMAP/RECYF
@creationdate: 2022/03/16
"""

import datetime as dt
import numpy as np
from reforecast_pearp_explorer import access
from reforecast_pearp_explorer.settings import ReforecastSettings

rfs = ReforecastSettings()

# Sizes of single files in Mo

vol_1nc_eurat01_f64 = 4.5
vol_1nc_eurat01_f32 = 3.0
vol_1nc_eurat01_f16 = 1.5
vol_1grib_eurat01 = 53.0

vol_1nc_glob025_f64 = 25
vol_1nc_glob025_f32 = 14
vol_1nc_glob025_f16 = 6
vol_1grib_glob025 = 151


n_files = rfs.estimate_storage_volume(1)
print("n_files=",n_files, "(number of files of a single type)")
voltot_grib = rfs.estimate_storage_volume(vol_1grib_eurat01 + vol_1grib_glob025)
print("Total volume of GRIB files:", np.round(voltot_grib/1024**2,2), "To")
voltot_nc64 = rfs.estimate_storage_volume(vol_1nc_eurat01_f64 + vol_1nc_glob025_f64)
print("Total volume of netCDF files with float64:", np.round(voltot_nc64/1024**2,2), "To")
voltot_nc32 = rfs.estimate_storage_volume(vol_1nc_eurat01_f32 + vol_1nc_glob025_f32)
print("Total volume of netCDF files with float32:", np.round(voltot_nc32/1024**2,2), "To")
voltot_nc16 = rfs.estimate_storage_volume(vol_1nc_eurat01_f16 + vol_1nc_glob025_f16)
print("Total volume of netCDF files with float16:", np.round(voltot_nc16/1024**2,2), "To")

rundate_start =  dt.datetime(2016,12,31,18)
rundate_end =  dt.datetime(2017,2,1,0)
vol = rfs.estimate_storage_volume(vol_1nc_glob025_f64, startdate=rundate_start, enddate=rundate_end)
print("NetCDF files from GLOB025 between", rundate_start, "and", rundate_end, np.round(vol/1024,2), "Go")

vol = rfs.estimate_storage_volume(vol_1nc_eurat01_f16 + vol_1nc_glob025_f16, leadtimes = [dt.timedelta(hours=3)])
print("Full period, one leadtime, float16:", np.round(vol/1024,2), "Go")

# Dates cyclone (Rolf)
# See doc of ./browse_rundates.py for dates of famous cyclones
rundate_start =  dt.datetime(2011,11,3,0)
rundate_end =  dt.datetime(2011,11,7,0)

closestrun = rfs.get_closest_rundate(rundate_start)

vol = rfs.estimate_storage_volume(vol_1nc_glob025_f64 + vol_1nc_eurat01_f64, startdate=rundate_start, enddate=rundate_end)
print("Cyclone of interest:", np.round(vol/1024,2), "Go")
