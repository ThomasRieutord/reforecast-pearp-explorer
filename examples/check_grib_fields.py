#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Tools to access the long-term PEARP reforecast made at CNRM/GMAP/RECYF.
Check elementary stats of the fields that are useful

@author: Rieutord Thomas
@creationdate: 2022/01/12
"""

import os
import datetime as dt
import epygram
from reforecast_pearp_explorer import gribs
from reforecast_pearp_explorer import access


testbasepath = "/cnrm/recyf/NO_SAVE/Data/users/rieutordt/test/GCQ7.3"
isgrib = lambda p: p.split(".")[-1]=="grib"
lgribs = list(filter(isgrib,os.listdir(testbasepath)))
lgribpath = [os.path.join(testbasepath,gb) for gb in lgribs]
# lgribpath = ["/cnrm/recyf/NO_SAVE/Data/users/rieutordt/test/GCQ7.1/grid.arpege-forecast.glob025+0009:00.grib"]

for gribpath in lgribpath:
    print("Path to grib:", gribpath)
    
    go = epygram.formats.resource(gribpath, openmode="r")
    try:
        allOK = gribs.check_reqfields(go)
    except epygram.epygramError:
        print("Epygram error. Probably some field missing")
    
# Debug field handgrips
#-----------------------

hg = {"discipline":0, "parameterCategory":3, "parameterNumber":6, "typeOfFirstFixedSurface":198, "scaledValueOfFirstFixedSurface":27315}
hg = {"discipline":0, "parameterCategory":0, "parameterNumber":3, "typeOfFirstFixedSurface":100}
n_matches = gribs.handgrip_matches(hg, go)
print(n_matches, "fields match with the handgrip", hg)
[print(f) for f in go.listfields(select=hg)]
gribs.display_fields_params(go, sel=hg, keys = ["discipline", "parameterCategory", "parameterNumber", "shortName", "name", "typeOfStatisticalProcessing", "level"])
