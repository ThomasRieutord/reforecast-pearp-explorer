#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Tools to access the long-term PEARP reforecast made at CNRM/GMAP/RECYF.
Make download from a prestaging file.

@author: Rieutord Thomas
@date: 2021/09/06
"""

import os
import time
import datetime as dt
from reforecast_pearp_explorer import hendrix

prstgfile = "prestaging_rieutordt_201810-201810_2y8zzg_0.txt"
outputDir = "../prestaging"

hendrix.ftpdownload(
    os.path.join(outputDir,prstgfile),
    submit=True,
    outputDir=outputDir,
    cleanup=True
)
