#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Tools for the exploration of the PEARP reforecast in cycle 43t2
Check if files exist in the local machine (LUSTRE)

@author: Rieutord Thomas
@date: 2021/09/06

DEPRECATED: will be removed in the next version
"""

import os
import time
import datetime as dt
from reforecast_pearp_explorer import graphics
from reforecast_pearp_explorer import access
from reforecast_pearp_explorer import utils
from reforecast_pearp_explorer import local

what = "reforecast"
domain = "eurat01"
# what = "reanalysis"
# domain = "tl1198-c22"
runhour = 18
startdate = dt.datetime(2016,3,1,runhour)
enddate = dt.datetime(2020,3,1,runhour)

xpid = utils.get_xpid_from_date(startdate.date(), what=what)

# title = "Files on LUSTRE for "+what+" from " + startdate.strftime("%x") + " to " + enddate.strftime("%x") + " ("+xpid+")"
title = "Files on LUSTRE for "+what+" from " + startdate.strftime("%x") + " to " + enddate.strftime("%x")
figname = "barplot_fileexist_byyear"

graphics.storeImages = True
graphics.figureDir = "../figures"

rd, oc, ec = graphics.barplot_fileexist(startdate, enddate, what, domain, groupby="year", title=title, figname=figname)

# Prepare re-download
#---------------------

local.prepare_redownload(xpid, domain, outputDir="../tmp")
