#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Tools to access the long-term PEARP reforecast made at CNRM/GMAP/RECYF.
Program to make rapid checks (whether fields are present in GRIB files or not)

@author: Rieutord Thomas (thomas.rieutord@meteo.fr)
@institution: Météo-France DESR/CNRM/GMAP/RECYF
@creationdate: 2022/02/22
"""

import os
import epygram
from reforecast_pearp_explorer import access
from reforecast_pearp_explorer import gribs
from reforecast_pearp_explorer.settings import ReforecastSettings

rfs = ReforecastSettings()


needers = ["LAERO", "CEN", "DOP", "EFI"]
default_pressure_levels = [1000, 850, 700, 500, 300]

needs = {
    "CEN":{
        "domains":["glob025", "eurat01"],
        "fields_to_check":[
            "height_at_0_wet_bulb_potential_temperature",
            "height_at_1_wet_bulb_potential_temperature",
            "precipitation_amount",
            "snowfall_amount",
        ] + rfs.add_vlevel_to_fieldnames(
            ["air_temperature", "wet_bulb_temperature"],
            default_pressure_levels
        )
    },
    "LAERO":{
        "domains":["glob025"],
        "fields_to_check":[
            "geopotential_height_at_1.5_PVU",
            "air_pressure_at_mean_sea_level",
        ]+ rfs.add_vlevel_to_fieldnames(
            [
                "eastward_wind",
                "northward_wind",
                "air_temperature",
                "geopotential",
                "relative_humidity",
                "air_potential_temperature",
                "potential_vorticity",
                "absolute_vorticity",
            ],
            default_pressure_levels
        ) + rfs.add_vlevel_to_fieldnames(
            ["divergence_of_wind"],
            [250,850]
        ) + rfs.add_vlevel_to_fieldnames(
            ["upward_air_velocity"],
            [400,600]
        )
    },
    "DOP":{
        "domains":["eurat01"],
        "fields_to_check":[
            "x_wind_gust_at_10_meters",
            "y_wind_gust_at_10_meters",
            "air_temperature_at_2_meters",
            "wet_bulb_potential_temperature_at_850_hPa",
            "cape_instantaneous",
        ] + rfs.add_vlevel_to_fieldnames(
            ["eastward_wind","northward_wind"],
            [10,100],
            "meters"
        )
    },
    "EFI":{
        "domains":["eurat01"],
        "fields_to_check":[
            "wind_speed_of_gust_at_10_meters",
            "air_temperature_at_2_meters",
            "precipitation_amount",
        ]
    },
}

# Fluxes are not computed at term +0h (could raise error)
fluxesfields = [
    "x_wind_gust_at_10_meters",
    "y_wind_gust_at_10_meters",
    "wind_speed_of_gust_at_10_meters",
    "snowfall_amount",
    "precipitation_amount",
    "surface_upward_latent_heat_flux",
    "surface_upward_sensible_heat_flux",
    "total_convective_precipitation_amount",
]

testbasepath = "/cnrm/recyf/NO_SAVE/Data/users/rieutordt/test/GCQ7.7"
isgrib = lambda p: p.split(".")[-1]=="grib"
lgribs = list(filter(isgrib,os.listdir(testbasepath)))
lgribpath = [os.path.join(testbasepath,gb) for gb in lgribs]
# lgribpath = ["/cnrm/recyf/NO_SAVE/Data/users/rieutordt/test/GCQ7.1/grid.arpege-forecast.glob025+0009:00.grib"]

fields_to_extract = rfs.fields_to_extract

n_errors = 0

for gribpath in lgribpath:
    print("\nPath to grib:", gribpath)
    domain = access.get_domain_from_filename(gribpath)
    
    go = epygram.formats.resource(gribpath, openmode="r")
    
    for usr in needers:
        usrDomains = needs[usr]["domains"]
        if not domain in usrDomains:
            print("Domain is ",domain," therefore the needs of ", usr, "cannot be checked.")
            continue
        
        fields_to_check = needs[usr]["fields_to_check"]
        for fd in fields_to_check:
            try:
                field = gribs.get_field(go, fd)
            except epygram.epygramError:
                term = int(gribpath[gribpath.index("+")+1:gribpath.index(":")])
                if fd in fluxesfields and term==0:
                    print("Field "+fd+" not computed at term +0H")
                else:
                    print("/!\ Error with the field "+fd+". Domain="+domain)
                    n_errors += 1
    
    for fd in fields_to_extract[domain]:
        try:
            field = gribs.get_field(go, fd)
        except epygram.epygramError:
            term = int(gribpath[gribpath.index("+")+1:gribpath.index(":")])
            if fd in fluxesfields and term==0:
                print("Field "+fd+" not computed at term +0H")
            else:
                print("/!\ Error with the field "+fd+". Domain="+domain)
                n_errors += 1
    
print("Checking of ", len(lgribpath),"GRIB files ended with ", n_errors, "errors")
hg = {"discipline":0, "parameterCategory":0, "parameterNumber":3, "typeOfFirstFixedSurface":100}
n_matches = gribs.handgrip_matches(hg, go)
print(n_matches, "fields match with the handgrip", hg)
[print(f) for f in go.listfields(select=hg)]
gribs.display_fields_params(go, sel=hg, keys = ["discipline", "parameterCategory", "parameterNumber", "shortName", "name", "typeOfStatisticalProcessing", "level"])
