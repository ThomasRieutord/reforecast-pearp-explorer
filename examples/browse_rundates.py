#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Tools to access the long-term PEARP reforecast made at CNRM/GMAP/RECYF.
Short program to browse the available rundates.

@author: Rieutord Thomas (thomas.rieutord@meteo.fr)
@institution: Météo-France DESR/CNRM/GMAP/RECYF
@creationdate: 2022/06/21

FAMOUS CYCLONES
===============

Rolf
-----

### Dates of the event
rundate_start =  dt.datetime(2011,11,6,0)
rundate_end =  dt.datetime(2011,11,9,0)

### Rundates boundaries covering the event
rundate_start =  dt.datetime(2011,11,3,0)
rundate_end =  dt.datetime(2011,11,7,0)


Zorbas
------

### Dates of the event
rundate_start =  dt.datetime(2018,9,27,0)
rundate_end =  dt.datetime(2018,9,30,0)

### Rundates boundaries covering the event
rundate_start =  dt.datetime(2018,9,24,0)
rundate_end =  dt.datetime(2018,9,28,0)

Ianos
-----
### Dates of the event
reqdate = dt.datetime(2020,9,18)


### Rundates boundaries covering the event
rundate_start =  dt.datetime(2020, 9, 12, 12)
rundate_end =  dt.(2020, 9, 18, 0)

"""

import datetime as dt
import numpy as np
from reforecast_pearp_explorer.settings import ReforecastSettings

rfs = ReforecastSettings()

reqdate = dt.datetime(2020,9,18)

print("Requested date:", reqdate)

print("  Closest rundate is",rfs.get_closest_rundate(reqdate))
print("  Closest anterior rundate is",rfs.get_closest_rundate(reqdate, position="anterior"))
print("  Closest posterior rundate is",rfs.get_closest_rundate(reqdate, position="posterior"))
print("  Closest anterior rundate for network 6Z is",rfs.get_closest_rundate(reqdate, position="posterior", network="06"))


print("Rundates with requested date in validity date:", rfs.get_rundates_between_dates(reqdate - dt.timedelta(hours=rfs.maxleadtime), reqdate))

rundate_start =  dt.datetime(2014,12,31,18)
rundate_end =  dt.datetime(2017,2,1,0)

print("\nBoundary dates:", rundate_start, rundate_end)
print(f"  There are {len(rfs.get_rundates_between_dates(rundate_start, rundate_end))} rundates in between,")
print(f"  including {len(rfs.get_rundates_between_dates(rundate_start, rundate_end, network='18'))} rundates from the 18Z network,")
print(f"  and {len(rfs.get_rundates_between_dates(rundate_start, rundate_end, months=[5,9]))} rundates in May or September")
