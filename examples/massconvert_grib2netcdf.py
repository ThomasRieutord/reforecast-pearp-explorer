#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Tools to access the long-term PEARP reforecast made at CNRM/GMAP/RECYF.
Convert multiple GRIB files in netCDF files following the CF conventions.

Link to CF conventions : http://cfconventions.org/Data/cf-standard-names/current/build/cf-standard-name-table.html

@author: Rieutord Thomas
@creationdate: 2022/01/31
"""

import os
import time
import datetime as dt
import numpy as np

from epygram import epygramError

from reforecast_pearp_explorer import gribs
from reforecast_pearp_explorer import access
from reforecast_pearp_explorer.settings import ReforecastSettings

rfs = ReforecastSettings().build()

n_members = rfs.n_members
leadtimes = rfs.leadtimes
time_between_runs = rfs.time_between_runs

rundate_start =  dt.datetime(2018,3,1,18)
rundate_end =  dt.datetime(2018,4,14,18)
domain = "glob025"
prefixdir="test"
borders = {"lonmin":-10, "latmin":25, "lonmax":40, "latmax":50}

n_rundates = int((rundate_end - rundate_start).total_seconds() // time_between_runs.total_seconds()) + 1
n_ldt = len(leadtimes)

# n_members = 2
# n_ldt = 2

for i_rundate in range(n_rundates):
    rundate = rundate_start + i_rundate*time_between_runs
    print("\n*** rundate =",rundate)
    for i_mb in range(n_members):
        print("   --- mb", i_mb," ---")
        for i_ldt in range(n_ldt):
            ldt = leadtimes[i_ldt]
            gribpath = access.get_lustrepath(
                domain=domain,
                rundate=rundate,
                member=i_mb,
                leadtime=ldt,
                prefixdir=prefixdir,
            )
            netcdfpath = access.get_lustrepath(
                domain=domain,
                rundate=rundate,
                member=i_mb,
                leadtime=ldt,
                prefixdir="netcdf",
            )[:-8] + ".nc"
            print("Exporting ", gribpath, "into ", netcdfpath)
            utils.check_lustrepath(netcdfpath, mkdir=True)
            
            gribs.drop_to_netcdf(gribpath, netcdfpath=netcdfpath, subdom_borders=borders)

# End of file
